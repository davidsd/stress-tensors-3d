#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 <file_with_newline_separated_list_of_directories_to_delete>"
    exit 1
fi

input_file="$1"

for x in $(cat "$input_file"); do
    echo "Deleting files in directory: $x"
    
    # Find and unlink all the files
    time lfs find "$x" -type f -print0 | xargs -0 -n 1 unlink
done

for x in $(cat "$input_file"); do
    echo "Deleting directory: $x"

    # Delete the directories
    time find -P "$x" -type d -empty -delete
done
