{-# LANGUAGE OverloadedStrings #-}

module StressTensors3d.Test.CompositeBlockMemory where

import Blocks                         (Block (..), Coordinate (..), Sign (..))
import Blocks.Blocks3d                (Block3dParams (..), ConformalRep (..),
                                       DebugLevel (..), Q4Struct (..))
import Blocks.Blocks3d                qualified as B3d
import Blocks.Blocks3d.CompositeBlock (CompositeBlockKey (..),
                                       ToSO3Structs (..),
                                       compositeBlockDependencies,
                                       writeCompositeBlockTable)
import Blocks.Blocks3d.ThreePtStruct  (ThreePtStruct (..))
import Config qualified
import Data.Binary                    (Binary (..))
import Data.Ratio                     ((%))
import Hyperion                       (Dict (..))
import Hyperion.Log                   qualified as Log
import Hyperion.Util.ToPath           (ToPath (..), dirScatteredHashBasePath)
import StressTensors3d.TTOStructLabel (TTOStructLabel (..))
import System.FilePath.Posix          ((<.>), (</>))
import Type.Reflection                (Typeable)

compositeBlockTestDir :: FilePath
compositeBlockTestDir = "/home/dsd/projects/stress-tensors-3d/composite_block_tests"

writeCompositeBlockDependencies :: ToSO3Structs t => CompositeBlockKey t -> IO ()
writeCompositeBlockDependencies key = do
  mapM_ (B3d.writeBlockTables blocks_3d_exec numThreads debugLevel blockDir) deps
  where
    deps = B3d.groupBlockTableKeys (compositeBlockDependencies key)
    blocks_3d_exec = Config.scriptsDir </> "blocks_3d.sh"
    numThreads = 1
    debugLevel = Debug
    blockDir = compositeBlockTestDir

writeCompositeBlock
  :: (Binary t, ToSO3Structs t, Typeable t, Show t, ToPath (CompositeBlockKey t))
  => CompositeBlockKey t
  -> IO ()
writeCompositeBlock = writeCompositeBlockTable Dict compositeBlockTestDir

myCompositeBlockKey :: CompositeBlockKey TTOStructLabel
myCompositeBlockKey = CompositeBlockKey
  { block =
      Block
      { struct12 =
          ThreePtStruct
          { rep1 =
              ConformalRep { delta = 3 % 1 , spin = 2 }
          , rep2 =
              ConformalRep { delta = 3 % 1 , spin = 2 }
          , rep3 = ConformalRep { delta = () , spin = 24 }
          , label = GenericParityEven2 24
          }
      , struct43 =
          ThreePtStruct
          { rep1 =
              ConformalRep { delta = 3 % 1 , spin = 2 }
          , rep2 =
              ConformalRep { delta = 3 % 1 , spin = 2 }
          , rep3 = ConformalRep { delta = () , spin = 24 }
          , label = GenericParityEven2 24
          }
      , fourPtFunctional =
        Q4Struct
        { q4qs = ( 1 , -1 , -1 , 1 ) , q4Sign = Plus }
      }
  , coordinate = XT_Radial
  , params =
      Block3dParams
      { nmax          = 4
      , order         = 30
      , keptPoleOrder = 30
      , precision     = 1024
      }
  }

instance ToPath (CompositeBlockKey TTOStructLabel) where
  toPath dir k = dir </> dirScatteredHashBasePath "tto_block" k <.> "dat"

-- Before 6/14/24, there was a performance bug whereby all Block3d
-- files were read first, before being parsed and added together. That
-- bug has hopefully now been fixed
-- (https://gitlab.com/davidsd/blocks-3d/-/commit/309db018c1cd8293962fc32dab8a050cabc559dc). Before
-- the bug, if we comment out the "writeCompositeBlockDependencies
-- myCompositeBlockKey" line (after computing the dependencies), this
-- would run with Max Resident Set Size ~49 MB. After fixing the bug,
-- it has Max RSS ~23 MB.
--
-- You can see detailed memory usage using the eventlog, see this
-- tutorial: https://www.youtube.com/watch?v=nIyaC3JtlyQ
--
-- In this case, run: stress-tensors-3d-test +RTS -s -l -hT
--
-- This will generate stress-tensors-3d-test.eventlog, on which you
-- can run eventlog2html.
writeMyCompositeBlock :: IO ()
writeMyCompositeBlock = do
  -- Log.info "Writing CompositeBlock dependencies" myCompositeBlockKey
  -- writeCompositeBlockDependencies myCompositeBlockKey
  Log.text "Writing CompositeBlock"
  writeCompositeBlock myCompositeBlockKey
