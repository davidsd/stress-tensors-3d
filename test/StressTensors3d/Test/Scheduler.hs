{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Test.Scheduler where

import Control.Concurrent                     (threadDelay)
import Control.Monad                          (forM, void)
import Control.Monad.IO.Class                 (liftIO)
import Data.Aeson                             qualified as Aeson
import Data.Foldable                          qualified as Foldable
import Data.List                              (isPrefixOf)
import Data.List.NonEmpty                     qualified as NonEmpty
import Data.Map.Strict                        (Map)
import Data.Map.Strict                        qualified as Map
import Data.Maybe                             (isNothing)
import Data.Text                              (Text)
import Data.Time.Clock                        (NominalDiffTime)
import Data.Tree                              (Tree)
import Data.Tree                              qualified as Tree
import Hyperion                               (WorkerAddr (..))
import Hyperion.Bootstrap.Bound               (Bound (..), defaultBoundFiles)
import Hyperion.Util                          (nominalDiffTimeToMicroseconds)
import StressTensors3d.Programs.Defaults      (taskStatsDir)
import StressTensors3d.Programs.SkydiveNmax18 (nmax18SkydiveStartBound)
import StressTensors3d.Scheduler              (CanRemoteRunTask (..),
                                               HasTaskInfo (..), Node (..),
                                               NumCPUs, Tag, Task,
                                               TaskInfo (..), mkTask,
                                               runPurePathExists, taskRuntime,
                                               unDummyTask, unsafeEncodeOsPath)
import StressTensors3d.Scheduler.CriticalPath (TaskStory (..), minThreadsAlloc,
                                               refineAllocationCriticalPath)
import StressTensors3d.Scheduler.TaskGraph    (TaskGraph)
import StressTensors3d.Scheduler.TaskGraph    qualified as TaskGraph
import StressTensors3d.Scheduler.TSigEps      (FilesystemModel (..),
                                               boundTaskTree,
                                               buildBoundsSimulated)
import StressTensors3d.Scheduler.TSigEpsStats (collectTSigEpsTaskStats,
                                               readCollectedStats)
import StressTensors3d.Test.PartChunkWriting  (dExtNmax10, lambdaNmax10,
                                               myBound', myBoundFiles)
import StressTensors3d.TSigEps3d              (TSigEps3d (..))
import StressTensors3d.TTTT3d                 (ttttBlockDir)
import System.FilePath.Posix                  ((</>))
import System.OsPath                          (OsPath, splitDirectories)

second :: NominalDiffTime
second = 1

inTTTTBlockDir :: OsPath -> Bool
inTTTTBlockDir = \p -> ttttDirs `isPrefixOf` splitDirectories p
  where
    ttttDirs = splitDirectories (unsafeEncodeOsPath ttttBlockDir)

-- Check whether a path corresponds to a part chunk from the TTTT
-- equations.
isTTTTChunk :: OsPath -> Bool
isTTTTChunk p = any (`elem` pDirectories)
  [ unsafeEncodeOsPath ("chunk_" <> show k) | k <- [0 .. 17 :: Int] ]
  where
    pDirectories = splitDirectories p

testSimulateBuild :: IO ()
testSimulateBuild = do
  -- Simulates the situation where we have already built the TTTT
  -- blocks and part chunks.
  let
    fakePathExists p = inTTTTBlockDir p || isTTTTChunk p
    bound1 = myBound' dExtNmax10 lambdaNmax10
    boundsWithFiles = pure (bound1, myBoundFiles)
  -- Run the program "TSigEps_test_nmax6_scheduler" to generate this stats file
  statsFromBuild <- readCollectedStats
    [taskStatsDir </> "TSigEps_test_nmax6_scheduler_expanse_1_128_collected_stats.json"]
  taskStats <- buildBoundsSimulated 20 (5*second) statsFromBuild (PureFilesystem fakePathExists)
               (fakeNodes 1) boundsWithFiles
  Aeson.encodeFile "taskStats.json" taskStats
  Aeson.encodeFile "taskStatsCollected.json" $
    collectTSigEpsTaskStats bound1.precision (map (fmap unDummyTask) taskStats)

-- minThreads = if B3d.isRadialKey b then 5 else 10  => 43.64s
-- minThreads = if B3d.isRadialKey b then 10 else 15 => 60.43s
-- minThreads = if B3d.isRadialKey b then 3 else 8   => 40.77s
-- minThreads = if B3d.isRadialKey b then 1 else 5   => 39.54s
-- minThreads = 1                                    => 39.48s
testSimulateBuildNmax18 :: IO ()
testSimulateBuildNmax18 = do
  let
    fakePathExists p = inTTTTBlockDir p || isTTTTChunk p
    boundFiles = defaultBoundFiles "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0"
  statsFromBuild <- readCollectedStats
    ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/json/collected_stats_combined_rY7cVMLgO81TUnhMYgbPRwQtFtss9LIDLlHkbQNGWqw.json"]
    -- ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0/json/task_stats_collected_lEKhGmDpWY5U3uUd7c0rY6NJC9l-l3QbWnlx3dhcz84.json"]
    --[taskStatsDir </> "TSigEps_test_nmax18_scheduler_expanse_10_128_collected_stats_order_106.json"]
  void $
    buildBoundsSimulated 40 (0.5*second) statsFromBuild (PureFilesystem fakePathExists)
    (fakeNodes 10) (pure (nmax18SkydiveStartBound, boundFiles))

getTestGraphNmax18 :: IO (TaskGraph Task)
getTestGraphNmax18 = do
  let
    fakePathExists p = inTTTTBlockDir p || isTTTTChunk p
    boundFiles' = defaultBoundFiles "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0"
    boundsWithFiles = pure (nmax18SkydiveStartBound, boundFiles')
  stats <- readCollectedStats
    ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/EqEyq/Object_6VmneNdYXlPUeMft2i0wQ06mc2axhwROk-j7n4JMjF0/json/task_stats_collected_lEKhGmDpWY5U3uUd7c0rY6NJC9l-l3QbWnlx3dhcz84.json"]
    --[taskStatsDir </> "TSigEps_test_nmax18_scheduler_expanse_10_128_collected_stats_order_106.json"]
  let
    trees =
      runPurePathExists fakePathExists $
      forM boundsWithFiles $ \(bound, boundFiles) ->
      boundTaskTree stats bound boundFiles
    taskTree = Tree.Node (mkTask ()) (NonEmpty.toList trees)
    taskGraph = TaskGraph.fromTree taskTree
  pure taskGraph

getTestGraph :: IO (TaskGraph Task)
getTestGraph = do
  -- Simulates the situation where we have already built the TTTT
  -- blocks and part chunks.
  let
    fakePathExists p = inTTTTBlockDir p || isTTTTChunk p
    bound1 = myBound' dExtNmax10 lambdaNmax10
    boundsWithFiles = pure (bound1, myBoundFiles)
  stats <- readCollectedStats
    [taskStatsDir </> "TSigEps_test_nmax6_scheduler_expanse_1_128_collected_stats.json"]
  let
    trees =
      runPurePathExists fakePathExists $
      forM boundsWithFiles $ \(bound, boundFiles) ->
      boundTaskTree stats bound boundFiles
    taskTree = Tree.Node (mkTask ()) (NonEmpty.toList trees)
    taskGraph = TaskGraph.fromTree taskTree
  pure taskGraph

allocationBeginning :: Maybe (Map Task NumCPUs, [TaskStory Task]) -> [(Text, NominalDiffTime, NumCPUs)]
allocationBeginning (Just (_, stories)) = do
  story <- filter (\story -> isNothing story.predecessor) stories
  case (taskInfo story.task).tag of
    Just tag -> pure (tag, taskRuntime story.task story.numCpus, story.numCpus)
    Nothing  -> []
allocationBeginning Nothing = error "No allocation"

testCriticalPathNmax18 :: IO ()
testCriticalPathNmax18 = do
  myGraph <- getTestGraphNmax18
  let (times, myResult) = refineAllocationCriticalPath (fakeNodes 10) myGraph (minThreadsAlloc myGraph) 3000
  putStrLn $ show times
  putStrLn $ show $ allocationBeginning myResult

--------- Dummy MyTask test ------------

data MyTask = MT Tag NominalDiffTime Int
  deriving (Eq, Ord, Show)

instance HasTaskInfo MyTask where
  taskInfo (MT tag cost mem) = MkTaskInfo
    { memory = mem * 1024 * 1024 * 1024
    , runtime = \threads -> cost / fromIntegral threads
    , maxThreads = const maxBound
    , minThreads = const $ round cost `div` 20 + 1
    , tag = Just tag
    }

instance CanRemoteRunTask MyTask where
  remoteRunTask _ numCpus (MT _ cost _) = do
    -- Log.info "Running task" (node.address, t, numCpus)
    liftIO $ threadDelay (nominalDiffTimeToMicroseconds cost `div` numCpus)
    pure Nothing
    -- Log.info "Finished with task" (node.address, t, numCpus)

myTaskTree :: Tree MyTask
myTaskTree =
  Tree.Node (MT "A" 5.1 5)
  [ Tree.Node (MT "B" 6.3 7) []
  , Tree.Node (MT "C" 8.2 9)
    [ Tree.Node (MT "D" 3.5 1) [] ]
  ]

myTaskGraph :: TaskGraph MyTask
myTaskGraph = TaskGraph.fromTree myTaskTree

fakeNodes :: Int -> [Node]
fakeNodes n = do
  i <- [0 .. n-1]
  let addr = "node_" <> show i
  pure $ MkNode
    { memory  = 256*1024*1024*1024
    , cpus    = 128
    , address = LocalHost addr
    }

myCpuAllocation :: Map.Map MyTask Int
myCpuAllocation = Map.fromList [ (t, 1) | t <- Foldable.toList myTaskTree]

{-
-- | A collection of tasks, together with a set of dependencies
myTasks :: Map MyTask (Set MyTask)
myTasks = Map.fromList $ do
  tag <- ["Foo", "Bar"]
  cost <- [1 .. 100]
  --cost <- [1 .. 20]
  mem <- [(cost `div` 2 + 1) .. (cost `div` 2 + 5)]
  let deps = case tag of
        "Bar" -> Set.fromList $ do
          depCost <- [(cost `div` 5 + 1) .. (cost `div` 4 + 1)]
          let depMem = depCost `div` 2 + 3
          pure $ MT "Foo" (fromIntegral depCost) depMem
        _ -> Set.empty
  pure $ (MT tag (fromIntegral cost) mem, deps)
-}


{-
testRunTasks :: IO ()
testRunTasks = void $ runJobLocal' $ runTasksOnNodes (1*second) (fakeNodes 8) myTasks

-}
