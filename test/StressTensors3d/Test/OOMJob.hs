{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}

-- | This module implements a Slurm job that is designed to run out of
-- memory. We would like to see what exceptions are thrown and what
-- gets recorded in the log files when this happens.

module StressTensors3d.Test.OOMJob where

import Config qualified
import Control.DeepSeq         (deepseq)
import Control.Monad.IO.Class  (liftIO)
import Control.Monad.Reader    (asks, local)
import Data.Map                qualified as Map
import Hyperion                (Cluster, Job, JobEnv (..), MPIJob (..),
                                WorkerAddr (..), remoteEvalJob,
                                remoteEvalOnWorker, setJobMemory, setJobTime,
                                setJobType, setSlurmPartition)
import Hyperion.Bootstrap.Main (hyperionBootstrapMain)
import Hyperion.Log            qualified as Log
import Hyperion.Util           (minute)

test :: IO ()
test = hyperionBootstrapMain Config.config Config.staticConfig (const masterProgram)

oomProcess :: IO ()
oomProcess = go (1 :: Integer)
  where
    makeListAndReport n =
      let xs = [0 .. n]
      in
        xs `deepseq`
        Log.info "Made a list of size and sum" (n, sum xs)
    go n = do
      makeListAndReport n
      go (2*n)

headNodeJob :: Job ()
headNodeJob = do
  launcherMap <- asks jobWorkerLauncherMap
  let
    remoteAddrs = do
      addr@(RemoteAddr _) <- map fst $ Map.toList launcherMap
      pure addr
    remoteAddr = case remoteAddrs of
      a : _ -> a
      _     -> error "Couldn't find any remote addrs"
  remoteEvalOnWorker remoteAddr (static (liftIO oomProcess))

masterProgram :: Cluster ()
masterProgram =
  local (setJobTime (20*minute) . setSlurmPartition "debug" . setJobMemory "1G" . setJobType (MPIJob 2 1)) $
  remoteEvalJob (static headNodeJob)
