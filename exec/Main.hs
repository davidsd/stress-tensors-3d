module Main where

import Config                                    (config, staticConfig)
import Hyperion.Bootstrap.Main                   (hyperionBootstrapMain,
                                                  tryAllPrograms)
import StressTensors3d.Programs.OPEScanNmax14 qualified
import StressTensors3d.Programs.OPEScanNmax18 qualified
import StressTensors3d.Programs.OPEScanNmax22 qualified
import StressTensors3d.Programs.OPEScanNmax26 qualified
import StressTensors3d.Programs.SkydiveNmax18 qualified
import StressTensors3d.Programs.SkydiveNmax6To14 qualified
import StressTensors3d.Programs.TEps2023 qualified
import StressTensors3d.Programs.TSig2023 qualified
import StressTensors3d.Programs.TSigEps2023 qualified
import StressTensors3d.Programs.TTTT2023 qualified

main :: IO ()
main = hyperionBootstrapMain config staticConfig $
  tryAllPrograms
  [ StressTensors3d.Programs.TTTT2023.boundsProgram
  , StressTensors3d.Programs.TSig2023.boundsProgram
  , StressTensors3d.Programs.TEps2023.boundsProgram
  , StressTensors3d.Programs.TSigEps2023.boundsProgram
  , StressTensors3d.Programs.SkydiveNmax6To14.boundsProgram
  , StressTensors3d.Programs.SkydiveNmax18.boundsProgram
  , StressTensors3d.Programs.OPEScanNmax14.boundsProgram
  , StressTensors3d.Programs.OPEScanNmax18.boundsProgram
  , StressTensors3d.Programs.OPEScanNmax22.boundsProgram
  , StressTensors3d.Programs.OPEScanNmax26.boundsProgram
  ]
