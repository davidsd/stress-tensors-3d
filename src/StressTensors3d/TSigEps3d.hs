{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TSigEps3d
  ( TSigEps3d(..)
  , Objective(..)
  , getMatExt
  , writeCrossingEqsTSigEps3d
  , writeOPECoeffsTSigEps3d
  , crossingEqs
  , crossingMatTSigEps3d
  ) where

import Blocks                        (BlockFetchContext, Coordinate (XT),
                                      CrossingMat, Delta (..), Derivative (..),
                                      TaylorCoeff (..), Taylors)
import Blocks.Blocks3d               (ConformalRep (..), Parity (..))
import Blocks.Blocks3d               qualified as B3d
import Bootstrap.Bounds              (BoundDirection, FourPointFunctionTerm,
                                      HasBlockParams (..),
                                      HasConstraintConfig (..), HasRep (..),
                                      OPECoefficient, OPECoefficientExternal,
                                      Spectrum, boundDirSign,
                                      defaultConstraintConfig,
                                      opeCoeffExternalSimple, opeCoeffGeneric_,
                                      opeCoeffIdentical_, scaleOPECoeff)
import Bootstrap.Bounds              qualified as Bounds
import Bootstrap.Build               (HasForce)
import Bootstrap.Math.FreeVect       (FreeVect, (*^))
import Bootstrap.Math.HalfInteger    (HalfInteger)
import Bootstrap.Math.Linear         (toV)
import Bootstrap.Math.VectorSpace    (Tensor, VectorSpace (..), zero)
import Bootstrap.Math.VectorSpace    qualified as VS
import Control.DeepSeq               (NFData)
import Data.Aeson                    (FromJSON, ToJSON)
import Data.Binary                   (Binary)
import Data.Functor.Compose          (Compose (..))
import Data.Matrix.Static            (Matrix)
import Data.Maybe                    (maybeToList)
import Data.Proxy                    (Proxy (..))
import Data.Reflection               (Reifies, reflect)
import Data.Tagged                   (Tagged)
import Data.Vector                   (Vector)
import GHC.Generics                  (Generic)
import GHC.TypeNats                  (KnownNat)
import Hyperion                      (Dict (..), Static (..))
import Hyperion.Bootstrap.Bound      (SDPFetchBuildConfig (..), ToSDP (..))
import Linear.V                      (V)
import SDPB qualified
import StressTensors3d.CrossingEqs   (crossingEqsStructSet, structsSSSS,
                                      structsTSSS, structsTTSS, structsTTTS,
                                      structsTTTT)
import StressTensors3d.OPECoeffUtils (writeOPECoeffs)
import StressTensors3d.TSig3d        (TSig3d, tsBuildChain, tsFetchConfig)
import StressTensors3d.TSigEpsSetup  (ExternalDims (..), ExternalOp (..),
                                      SymmetrySector (..), TSMixedBlock,
                                      TSMixedStruct, lambdaTTEps, lambda_B,
                                      lambda_F, listSpectrum, opeSS, opeSigEps,
                                      opeTS, opeTT, so3, symmetrySectorLabel,
                                      toTSMixedBlock, wardSST)
import StressTensors3d.TTTT3d        (withNonEmptyVec)
import StressTensors3d.Z2Rep         (Z2Rep (..))
import Type.Reflection               (Typeable)

data TSigEps3d = TSigEps3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  , numPoles     :: Maybe Int
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, HasBlockParams B3d.Block3dParams)

data Objective
  = Feasibility (Maybe (V 5 Rational))
  | ExternalOPEBound (V 5 Rational) BoundDirection
  | NavigatorBound (Maybe (V 5 Rational))
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

type TSigEpsNumCrossing =  53

instance HasConstraintConfig TSigEpsNumCrossing TSigEps3d where
  type Deriv TSigEps3d = TaylorCoeff (Derivative 'XT)
  getConstraintConfig = defaultConstraintConfig (Proxy @B3d.Block3dParams) crossingEqs

-- | Crossing equations for the T-Sig-Eps system. The dimension of the
-- functional is dim(alpha) = 5 + 33*nmax + 15*nmax^2
--
-- nmax | dim(alpha)
-- -----------------
-- 6    | 743
-- 10   | 1835
-- 14   | 3407
-- 18   | 5459
-- 22   | 7991
-- 26   | 11003
crossingEqs
  :: forall s b a . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TSigEpsNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,T,Eps),       structsTTTS)
  , ((T,T,Sig,Sig),     structsTTSS)
  , ((Sig,T,Sig,T),     structsTTSS)
  , ((T,T,Eps,Eps),     structsTTSS)
  , ((Eps,T,Eps,T),     structsTTSS)
  , ((Eps,T,Eps,Eps),   structsTSSS)
  , ((Eps,T,Sig,Sig),   structsTSSS)
  , ((Sig,T,Sig,Eps),   structsTSSS)
  , ((Sig,Sig,Sig,Sig), structsSSSS)
  , ((Eps,Eps,Eps,Eps), structsSSSS)
  , ((Sig,Sig,Eps,Eps), structsSSSS)
  , ((Sig,Eps,Sig,Eps), structsSSSS)
  ]

-- | Write the crossing equations to a file in TeX format
writeCrossingEqsTSigEps3d :: FilePath -> IO ()
writeCrossingEqsTSigEps3d path = Bounds.writeCrossingEqs path (crossingEqs @())

-- | Write the OPE coefficients to a file in TeX format
writeOPECoeffsTSigEps3d :: FilePath -> TSigEps3d -> IO ()
writeOPECoeffsTSigEps3d path t = writeOPECoeffs path ope opeExt t

-- | TODO: Find blocks where the structures s12 and s43 have different
-- internal operators and set them to zero?
crossingMatTSigEps3d
  :: forall s j a. (KnownNat j, Fractional a, Eq a)
  => V j (OPECoefficient (ExternalOp s) TSMixedStruct a)
  -> (Tagged s `Tensor` CrossingMat j TSigEpsNumCrossing TSMixedBlock) a
crossingMatTSigEps3d channel =
  Bounds.tagVec $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrix channel (crossingEqs @s)

ope
  :: (Fractional a, Eq a, Reifies s TSigEps3d)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> concat
    [ opeTT r parity
    , opeSS Sig r parity
    , opeSS Eps r parity
    , opeTS Eps r parity
    ]
  Z2Odd  -> opeTS Sig r parity ++ opeSigEps r parity

gff
  :: forall a s. (Eq a, Floating a, Reifies s TSigEps3d)
  => (Tagged s `Tensor` CrossingMat 1 TSigEpsNumCrossing TSMixedBlock) a
gff = VS.sum $
  map (crossingMatTSigEps3d . toV . uncurry scaleOPECoeff) opeData
  where
    dSig = (rep (Sig @s)).delta
    dEps = (rep (Eps @s)).delta
    identicalS2 delta' = let delta = fromRational delta' in
      (sqrt (2/3)) * delta * (sqrt ((2 * (delta + 1))/((2*delta + 1))))
    opeData =
      [ (sqrt 2,           opeCoeffIdentical_ Sig (ConformalRep (2*dSig) 0) (so3 0 0))
      , (sqrt 2,           opeCoeffIdentical_ Eps (ConformalRep (2*dEps) 0) (so3 0 0))
      , (1,                opeCoeffGeneric_ Sig Eps (ConformalRep (dSig+dEps) 0) (so3 0 0) (so3 0 0))
      , (identicalS2 dSig, opeCoeffIdentical_ Sig (ConformalRep (2*dSig+2) 2) (so3 0 2))
      , (identicalS2 dEps, opeCoeffIdentical_ Eps (ConformalRep (2*dEps+2) 2) (so3 0 2))
      ]

bulk
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m
     , BlockFetchContext TSMixedBlock a m, Reifies s TSigEps3d
     )
  => [Tagged s (SDPB.PositiveConstraint m a)]
bulk = do
  let key = reflect @s Proxy
  (delta, range, sym@(SymmetrySector j parity z2)) <- listSpectrum key.spins key.spectrum
  let
    opeCoeffs = ope (ConformalRep delta j) parity z2
    label     = symmetrySectorLabel sym
  maybeToList $ withNonEmptyVec opeCoeffs $
    Bounds.constraint range label .
    crossingMatTSigEps3d

-- | See the Note in 'StressTensors3d.TSigEpsSetup' about how we handle
-- Ward identities. The external OPE coefficients are:
--
-- (<TTT>_B, <TTT>_F, <SSE>, <EEE>, <TTE>)
opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s TSigEps3d)
  => V 5 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( sumOPE [lambda_B, wardSST Sig, wardSST Eps]
  , sumOPE [lambda_F, wardSST Sig, wardSST Eps]
  , opeCoeffExternalSimple Sig Sig Eps (so3 0 0)
  , opeCoeffExternalSimple Eps Eps Eps (so3 0 0)
  , lambdaTTEps
  )
  where
    sumOPE = Bounds.fromOpeExtVS . VS.sum . map Bounds.toOpeExtVS

ext
  :: forall a s . (Fractional a, Eq a,  Reifies s TSigEps3d)
  => (Tagged s `Tensor` CrossingMat 5 TSigEpsNumCrossing TSMixedBlock) a
ext =
  Bounds.tagVec $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Sig,Eps]

getMatExt
  :: (BlockFetchContext TSMixedBlock a m, Applicative m, HasForce m, RealFloat a, NFData a)
  => TSigEps3d
  -> m (Matrix 5 5 (Vector a))
getMatExt key =
  Bounds.getIsolatedMat
  (getBlockParams key)
  (getConstraintConfig key).derivs
  (Bounds.runTagged key (getCompose ext))

extConstraint
  :: ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m
     , BlockFetchContext TSMixedBlock a m
     , Reifies s TSigEps3d
     )
  => Maybe (V 5 Rational)
  -> Tagged s (SDPB.PositiveConstraint m a)
extConstraint = \case
  Just lambda -> Bounds.constConstraint "ext" (Bounds.pair lambda ext)
  Nothing     -> Bounds.constConstraint "ext" ext

unit
  :: forall s a . (Reifies s TSigEps3d , Fractional a, Eq a)
  => (Tagged s `Tensor` CrossingMat 1 TSigEpsNumCrossing B3d.IdentityBlock3d) a
unit = Bounds.tagVec $ B3d.identityCrossingMat (crossingEqs @s)

tSigEps3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m, BlockFetchContext TSMixedBlock a m)
  => TSigEps3d
  -> SDPB.SDP m a
tSigEps3dSDP key = Bounds.runTagged key $
  case key.objective of
    Feasibility mLambda ->
      Bounds.sdp (zero `asTypeOf` unit) unit (extConstraint mLambda : bulk)

    ExternalOPEBound lambda direction ->
      Bounds.sdp unit lambdaExt bulk
      where
        lambdaExt = boundDirSign direction *^ Bounds.pair lambda ext

    NavigatorBound mLambda ->
      Bounds.sdp unit ((-1) VS.*^ gff) (extConstraint mLambda : bulk)

instance ToSDP TSigEps3d where
  type SDPFetchKeys TSigEps3d = SDPFetchKeys TSig3d
  toSDP = tSigEps3dSDP

instance SDPFetchBuildConfig TSigEps3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TSigEps3d)              where closureDict = static Dict
instance Static (Show TSigEps3d)                where closureDict = static Dict
instance Static (ToSDP TSigEps3d)               where closureDict = static Dict
instance Static (ToJSON TSigEps3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TSigEps3d) where closureDict = static Dict
