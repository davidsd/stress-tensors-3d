{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveAnyClass       #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedRecordDot  #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE StaticPointers       #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE UndecidableInstances #-}

module StressTensors3d.TEps3d
  ( TEps3d(..)
  , Objective(..)
  , getMatExt
  ) where

import Blocks                       (BlockFetchContext, Coordinate (XT),
                                     CrossingMat, Delta, Derivative (..),
                                     TaylorCoeff (..), Taylors)
import Blocks.Blocks3d              (ConformalRep (..), Parity (..))
import Blocks.Blocks3d              qualified as B3d
import Bootstrap.Bounds             (FourPointFunctionTerm, HasBlockParams,
                                     HasConstraintConfig, OPECoefficient,
                                     OPECoefficientExternal, Spectrum,
                                     addOPECoeffExt, defaultConstraintConfig,
                                     opeCoeffExternalSimple)
import Bootstrap.Bounds             qualified as Bounds
import Bootstrap.Build              (HasForce)
import Bootstrap.Math.FreeVect      (FreeVect)
import Bootstrap.Math.HalfInteger   (HalfInteger)
import Bootstrap.Math.Linear        (toV)
import Bootstrap.Math.VectorSpace   (Tensor, zero)
import Control.DeepSeq              (NFData)
import Data.Aeson                   (FromJSON, ToJSON)
import Data.Binary                  (Binary)
import Data.Functor.Compose         (getCompose)
import Data.Matrix.Static           (Matrix)
import Data.Maybe                   (maybeToList)
import Data.Proxy                   (Proxy (..))
import Data.Reflection              (Reifies, reflect)
import Data.Tagged                  (Tagged)
import Data.Vector                  (Vector)
import GHC.Generics                 (Generic)
import Hyperion                     (Dict (..), Static (..))
import Hyperion.Bootstrap.Bound     (SDPFetchBuildConfig (..), ToSDP (..))
import Linear.V                     (V)
import SDPB qualified
import StressTensors3d.CrossingEqs  (crossingEqsStructSet, structsSSSS,
                                     structsTSSS, structsTTSS, structsTTTS,
                                     structsTTTT)
import StressTensors3d.TSig3d       (TSig3d, tsBuildChain, tsFetchConfig)
import StressTensors3d.TSigEpsSetup (ExternalDims (..), ExternalOp (..),
                                     SymmetrySector (..), TSMixedBlock,
                                     TSMixedStruct, lambdaTTEps, lambda_B,
                                     lambda_F, listSpectrum, opeSS, opeTS,
                                     opeTT, so3, toTSMixedBlock, wardSST)
import StressTensors3d.TTTT3d       (withNonEmptyVec)
import StressTensors3d.Z2Rep        (Z2Rep (..))
import Type.Reflection              (Typeable)

data TEps3d = TEps3d
  { externalDims :: ExternalDims
  , spectrum     :: Spectrum SymmetrySector
  , objective    :: Objective
  , spins        :: [HalfInteger]
  , blockParams  :: B3d.Block3dParams
  , numPoles     :: Maybe Int
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, HasBlockParams B3d.Block3dParams)

data Objective
  = Feasibility (Maybe (V 4 Rational))
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance HasConstraintConfig TEpsNumCrossing TEps3d where
  type Deriv TEps3d = TaylorCoeff (Derivative 'XT)
  getConstraintConfig = defaultConstraintConfig (Proxy @B3d.Block3dParams) crossingEqs

type TEpsNumCrossing = 37

-- | Crossing equations for the T-Eps system
crossingEqs
  :: forall s b a . (Ord b, Num a, Eq a)
  => FourPointFunctionTerm (ExternalOp s) B3d.Q4Struct b a
  -> V TEpsNumCrossing (Taylors 'XT, FreeVect b a)
crossingEqs = crossingEqsStructSet
  [ ((T,T,T,T),         structsTTTT)
  , ((T,T,T,Eps),       structsTTTS)
  , ((T,T,Eps,Eps),     structsTTSS)
  , ((Eps,T,Eps,T),     structsTTSS)
  , ((Eps,T,Eps,Eps),   structsTSSS)
  , ((Eps,Eps,Eps,Eps), structsSSSS)
  ]

ope
  :: (Fractional a, Eq a, Reifies s TEps3d)
  => ConformalRep Delta
  -> Parity
  -> Z2Rep
  -> [OPECoefficient (ExternalOp s) TSMixedStruct a]
ope r parity z2 = case z2 of
  Z2Even -> concat
    [ opeTT r parity
    , opeSS Eps r parity
    , opeTS Eps r parity
    ]
  Z2Odd  -> []

bulk
  :: forall a s m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m
     , BlockFetchContext TSMixedBlock a m, Reifies s TEps3d)
  => [Tagged s (SDPB.PositiveConstraint m a)]
bulk = do
  let t = reflect @s Proxy
  (delta, range, SymmetrySector j parity z2) <- listSpectrum t.spins t.spectrum
  let opeCoeffs = ope (ConformalRep delta j) parity z2
  maybeToList $ withNonEmptyVec opeCoeffs $ \opeCoeffs' ->
    Bounds.constraint range "label" $
    Bounds.tagVec $
    Bounds.mapBlocks toTSMixedBlock $
    Bounds.crossingMatrix opeCoeffs' (crossingEqs @s)

-- | (lambda_B, lambda_F, <EEE>, <TTE>)
opeExt
  :: forall a s . (Eq a, Fractional a, Reifies s TEps3d)
  => V 4 (OPECoefficientExternal (ExternalOp s) TSMixedStruct a)
opeExt = toV
  ( lambda_B `addOPECoeffExt` wardSST Eps
  , lambda_F `addOPECoeffExt` wardSST Eps
  , opeCoeffExternalSimple Eps Eps Eps (so3 0 0)
  , lambdaTTEps
  )

ext
  :: forall a s . (Fractional a, Eq a,  Reifies s TEps3d)
  => (Tagged s `Tensor` CrossingMat 4 TEpsNumCrossing TSMixedBlock) a
ext =
  Bounds.tagVec $ Bounds.mapBlocks toTSMixedBlock $
  Bounds.crossingMatrixExternal (opeExt @a @s) crossingEqs [T,Eps]

getMatExt
  :: (BlockFetchContext TSMixedBlock a m, Applicative m, HasForce m, RealFloat a, NFData a)
  => TEps3d
  -> m (Matrix 4 4 (Vector a))
getMatExt t =
  Bounds.getIsolatedMat
  (Bounds.getBlockParams t)
  (Bounds.getConstraintConfig t).derivs
  (Bounds.runTagged t (getCompose ext))

unit
  :: forall s a . (Reifies s TEps3d , Fractional a, Eq a)
  => (Tagged s `Tensor` CrossingMat 1 TEpsNumCrossing B3d.IdentityBlock3d) a
unit = Bounds.tagVec $ B3d.identityCrossingMat (crossingEqs @s)

tEps3dSDP
  :: forall a m.
     ( Binary a, Typeable a, RealFloat a, NFData a, Applicative m, HasForce m, BlockFetchContext TSMixedBlock a m)
  => TEps3d
  -> SDPB.SDP m a
tEps3dSDP key = Bounds.runTagged key $
  case key.objective of
    Feasibility mLambda ->
      Bounds.sdp (zero `asTypeOf` unit) unit (extConstraint : bulk)
      where
        extConstraint = case mLambda of
          Just lambda -> Bounds.constConstraint "ext" (Bounds.pair lambda ext)
          Nothing     -> Bounds.constConstraint "ext" ext

instance ToSDP TEps3d where
  type SDPFetchKeys TEps3d = SDPFetchKeys TSig3d
  toSDP = tEps3dSDP

instance SDPFetchBuildConfig TEps3d where
  sdpFetchConfig _ _ = tsFetchConfig
  sdpDepBuildChain _ = tsBuildChain

instance Static (Binary TEps3d)              where closureDict = static Dict
instance Static (Show TEps3d)                where closureDict = static Dict
instance Static (ToSDP TEps3d)               where closureDict = static Dict
instance Static (ToJSON TEps3d)              where closureDict = static Dict
instance Static (SDPFetchBuildConfig TEps3d) where closureDict = static Dict

