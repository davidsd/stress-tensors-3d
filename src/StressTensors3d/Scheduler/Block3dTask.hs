{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.Block3dTask where

import Blocks                                    (Coordinate,
                                                  CoordinateDir (YDir), Sign)
import Blocks.Blocks3d                           qualified as B3d
import Blocks.Blocks3d.BlockTableKey             (BlockTableKey' (..))
import Blocks.Blocks3d.Build                     qualified as B3d
import Bootstrap.Math.HalfInteger                (HalfInteger)
import Control.DeepSeq                           (NFData)
import Control.Monad                             (void)
import Control.Monad.IO.Class                    (liftIO)
import Control.Monad.Reader                      (Reader, asks, runReader)
import Control.Monad.Writer                      (Writer, runWriter, tell)
import Data.Aeson                                (FromJSON, FromJSONKey, ToJSON,
                                                  ToJSONKey)
import Data.Binary                               (Binary)
import Data.Functor.Compose                      (Compose (..))
import Data.Map.Strict                           qualified as Map
import Data.Rendered                             (value)
import Data.Set                                  (Set)
import Data.Set                                  qualified as Set
import Data.Void                                 (Void)
import GHC.Generics                              (Generic)
import Hyperion                                  (cAp, cPure)
import Hyperion.Bootstrap.Bound                  (BoundConfig (..),
                                                  BoundFiles (..))
import StressTensors3d.Scheduler.MonadPathExists (MonadPathExists (..),
                                                  unsafeEncodeOsPath)
import StressTensors3d.Scheduler.RunTasks        (CanRemoteRunTask (..),
                                                  remoteRunOnNode)
import StressTensors3d.Scheduler.Task            (HasTaskHash (..),
                                                  afterReturnMemory)
import StressTensors3d.Scheduler.TaskInfo        (HasTaskInfo (..),
                                                  RunStage (..), TaskInfo (..))
import StressTensors3d.Scheduler.TaskLink        (TaskLink (..))
import System.FilePath.Posix                     ((</>))

data Block3dTask = MkBlock3dTask
  { key            :: BlockTableKey' (Set HalfInteger)
  , blockDir       :: FilePath
  , executablePath :: FilePath
  } deriving (Generic, Binary, ToJSON, HasTaskHash)

instance HasTaskInfo Block3dTask where
  taskInfo (MkBlock3dTask b _ _) = MkTaskInfo
    { memory     = round (B3d.blockMemoryApprox b)
    , runtime    = \numCpus -> realToFrac (1.7e-6 * B3d.blockCostApprox b / fromIntegral numCpus)
    , maxThreads = const maxBound
    -- Setting a nontrivial minThreads is sometimes necessary to
    -- prevent smaller tasks from crowding out the blocks_3d
    -- tasks. This seems to be needed at nmax=6 with 1 node. It's also
    -- needed for the initial skydive run at nmax=18, where many
    -- PartChunk's can be built at the beginning using pre-computed
    -- TTTT blocks. It may slow things down in later skydive
    -- iterations.
    -- , minThreads = 1
    , minThreads = \case
        InitialRun -> 1
        InProgressRun
          | b.lambda >= 43 -> if B3d.isRadialKey b then 16 else 32
          | b.lambda >= 35 -> if B3d.isRadialKey b then 10 else 20
          | b.lambda >= 11 -> if B3d.isRadialKey b then 5  else 10
          | otherwise      -> 1
    , tag        = Just "Block3d"
    }

instance CanRemoteRunTask Block3dTask where
  remoteRunTask node numCpus task =
    remoteRunOnNode node numCpus $
    afterReturnMemory $
    cAp (static liftIO) $
    static B3d.writeBlockTables
    `cAp` cPure task.executablePath
    `cAp` cPure numCpus
    `cAp` cPure B3d.Debug
    `cAp` cPure task.blockDir
    `cAp` cPure task.key

type Block3dGrouper a = Compose
  (Writer (Set B3d.BlockTableKey))
  (Reader (B3d.BlockTableKey -> BlockTableKey' (Set HalfInteger)))
  a

-- | Collect all B3d.BlockTableKey's, group them together into spin
-- sets, build a map from groups to Tasks, and replace all
-- BlockTableKey's with the task for their group.
runBlock3dGrouper :: Block3dGrouper a -> a
runBlock3dGrouper mRW = case runWriter (getCompose mRW) of
  (mR, allBlock3ds) ->
    let
      b3dGroupMap = Map.fromList $ do
        group <- B3d.groupBlockTableKeys allBlock3ds
        pure (void group, group)
    in
      runReader mR $ \b -> b3dGroupMap Map.! void b

block3dTaskLink
  :: MonadPathExists m
  => BoundConfig
  -> BoundFiles
  -> TaskLink m B3d.BlockTableKey Void (Block3dGrouper Block3dTask)
block3dTaskLink boundConfig boundFiles = MkTaskLink
  { dependencies = const Set.empty
  , checkCreated = doesPathExist . unsafeEncodeOsPath . B3d.blockTableFilePath boundFiles.blockDir
  , toTask       = mkBlock3dTask
  }
  where
    blocks3dExecutable = scriptsDir boundConfig </> "blocks_3d.sh"

    mkBlock3dTask :: B3d.BlockTableKey -> Block3dGrouper Block3dTask
    mkBlock3dTask b = Compose $ do
      tell (Set.singleton b)
      pure $ do
        blockGroup <- asks ($ b)
        pure $ MkBlock3dTask blockGroup boundFiles.blockDir blocks3dExecutable

data ZeroOrNot = Zero | NotZero
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, NFData)

zeroOrNot :: (Eq a, Num a) => a -> ZeroOrNot
zeroOrNot x = if x == 0 then Zero else NotZero

data Block3dStatKey = MkBlock3dStatKey
  { jExternal     :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , jInternal     :: Set HalfInteger
  , j12           :: HalfInteger
  , j43           :: HalfInteger
  , delta12       :: ZeroOrNot
  , delta43       :: ZeroOrNot
  , delta1Plus2   :: ZeroOrNot
  , fourPtStruct  :: (HalfInteger, HalfInteger, HalfInteger, HalfInteger)
  , fourPtSign    :: Sign 'YDir
  , order         :: Int
  , lambda        :: Int
  , coordinates   :: Set.Set Coordinate
  , keptPoleOrder :: Int
  , precision     :: Int
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, ToJSONKey, FromJSONKey, NFData)

toBlock3dStatKey :: Block3dTask -> Block3dStatKey
toBlock3dStatKey task = MkBlock3dStatKey
  { jExternal     = task.key.jExternal
  , jInternal     = task.key.jInternal
  , j12           = task.key.j12
  , j43           = task.key.j43
  , delta12       = zeroOrNot (value task.key.delta12)
  , delta43       = zeroOrNot (value task.key.delta43)
  , delta1Plus2   = zeroOrNot (value task.key.delta1Plus2)
  , fourPtStruct  = task.key.fourPtStruct
  , fourPtSign    = task.key.fourPtSign
  , order         = task.key.order
  , lambda        = task.key.lambda
  , coordinates   = task.key.coordinates
  , keptPoleOrder = task.key.keptPoleOrder
  , precision     = task.key.precision
  }
