{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}

module StressTensors3d.Scheduler.TaskInfo where

import Debug.Trace qualified as Debug
import Data.Text                   (Text)
import Data.Time.Clock             (NominalDiffTime)

type NumCPUs = Int

type Tag = Text

-- | We allow minThreads and maxThreads to depend on the stage of the
-- computation. TODO: Really, minThreads and maxThreads should be able
-- to depend on stats. How do we achieve that?
data RunStage = InitialRun | InProgressRun

data TaskInfo = MkTaskInfo
  { -- | Estimated memory in bytes
    memory     :: Int
    -- | Estimated runtime in seconds, as a function of NumCPUs
  , runtime    :: NumCPUs -> NominalDiffTime
    -- | Maximum possible threads for the task
  , maxThreads :: RunStage -> NumCPUs
    -- | Minimum possible threads for the task
  , minThreads :: RunStage -> NumCPUs
    -- | A label indicating the type of task. If Nothing, the task
    -- will be ommitted from progress reports.
  , tag        :: Maybe Tag
  }

class HasTaskInfo a where
  taskInfo :: a -> TaskInfo

-- | Unit is sometimes useful as a top level placeholder task.
instance HasTaskInfo () where
  taskInfo _ = MkTaskInfo
    { memory     = 0
    , runtime    = const 0
    , maxThreads = const 0
    , minThreads = const 0
    , tag        = Nothing
    }

-- | Hard-coded maximum memory in bytes. TODO: Make this configurable.
nodeMaxMemory :: Int
nodeMaxMemory = 240 * 1024 * 1024 * 1024

taskMemory :: HasTaskInfo a => a -> Int
taskMemory t =
  let
    memEstimate = (taskInfo t).memory
  in
    if memEstimate > nodeMaxMemory
    then Debug.trace (concat
                       [ "WARNING: task memEstimate exceeds nodeMaxMemory."
                       , " Replacing it with nodeMaxMemory. This might lead to a crash."
                       , " memEstimate = ", show memEstimate
                       , ", nodeMaxMemory = ", show nodeMaxMemory
                       , ", task.tag = ", show (taskInfo t).tag
                       ]) nodeMaxMemory
    else memEstimate

taskRuntime :: HasTaskInfo a => a -> NumCPUs -> NominalDiffTime
taskRuntime t = (taskInfo t).runtime

taskMinThreads :: HasTaskInfo a => RunStage -> a -> NumCPUs
taskMinThreads stage t = (taskInfo t).minThreads stage

taskMaxThreads :: HasTaskInfo a => RunStage -> a -> NumCPUs
taskMaxThreads stage t = (taskInfo t).maxThreads stage
