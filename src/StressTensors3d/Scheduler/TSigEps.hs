{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.TSigEps where

import Blocks.Blocks3d                        (Block3dParams)
import Bootstrap.Build                        (SumDropVoid)
import Control.Monad                          (forM, when)
import Control.Monad.IO.Class                 (MonadIO, liftIO)
import Control.Monad.Reader                   (asks)
import Data.Aeson                             (ToJSON)
import Data.Binary                            (Binary)
import Data.List.NonEmpty                     (NonEmpty (..))
import Data.List.NonEmpty                     qualified as NonEmpty
import Data.Maybe                             (fromMaybe)
import Data.Proxy                             (Proxy (..))
import Data.Scientific                        (Scientific)
import Data.Set                               qualified as Set
import Data.Time.Clock                        (NominalDiffTime)
import Data.Tree                              (Tree)
import Data.Tree                              qualified as Tree
import GHC.Records                            (HasField)
import GHC.TypeNats                           (KnownNat)
import Hyperion                               (Cluster, Dict (..), Job,
                                               Static (..), cAp, cPure,
                                               clusterJobOptions, newWorkDir,
                                               remoteEvalJob, runJobLocal')
import Hyperion.Bootstrap.Bound               (Bound (..), BoundConfig (..),
                                               BoundFileTreatment,
                                               BoundFiles (..), Precision (..),
                                               SDPFetchBuildConfig,
                                               SDPFetchKeys, ToSDP,
                                               boundToSDPDeps,
                                               cleanFilesWithTreatment,
                                               sdpbInputFromBoundFiles,
                                               setFixedTimeLimit)
import Hyperion.Database                      qualified as DB
import Hyperion.Log                           qualified as Log
import Hyperion.Slurm                         qualified as Slurm
import Hyperion.Util.ToPath                   (hashBasePath)
import SDPB qualified
import StressTensors3d.Scheduler              (Block3dGrouper, DummyTask (..),
                                               MonadPathExists, Node (..), Task,
                                               TaskChain (..), TaskStats,
                                               block3dTaskLink, boundTaskLink,
                                               compositeBlockTaskLink, mkTask,
                                               mkTaskWithStats,
                                               runBlock3dGrouper,
                                               runMemoizedPathExists,
                                               runPurePathExists, runTasks,
                                               runTasksOnNodes,
                                               sdpPartChunkTaskLink,
                                               sdpPartTaskLink,
                                               toBlock3dStatKey,
                                               toCompositeBlockStatKey,
                                               toPartChunkStatKey,
                                               toPartStatKey, toTaskTree)
import StressTensors3d.Scheduler.TSigEpsStats (CollectedStats (..),
                                               collectTSigEpsTaskStats,
                                               encodeJsonFileAtomic,
                                               readCollectedStats,
                                               writeCollectedStats)
import StressTensors3d.TSigEps3d              (TSigEps3d (..))
import System.Directory                       (doesPathExist)
import System.FilePath.Posix                  (dropTrailingPathSeparator,
                                               replaceBaseName, (<.>), (</>))
import System.OsPath                          (OsPath)
import Type.Reflection                        (Typeable)

type HasTSigEpsDependencies b =
  ( SumDropVoid (SDPFetchKeys TSigEps3d) ~ SumDropVoid (SDPFetchKeys b)
  , HasField "blockParams" b Block3dParams
  )

type CanBuildBound p b =
  ( Static (ToSDP b)
  , Static (SDPFetchBuildConfig b)
  , Static (Binary b)
  , Static (Show b)
  , Static (ToJSON b)
  , Typeable b
  , KnownNat p
  -- TODO: Eventually, we would like to generalize this code so that
  -- it works for any Bound implementing a 'HasTaskChain' class.
  , HasTSigEpsDependencies b
  )

boundTaskChain
  :: (CanBuildBound p b, MonadPathExists m)
  => CollectedStats
  -> Bound (Precision p) b
  -> BoundFiles
  -> TaskChain m () (Block3dGrouper Task)
boundTaskChain stats bound boundFiles =
  TaskNode (pure . mkTask          <$> boundTaskLink        bound boundFiles) $
  TaskNode (pure . mkPartTask      <$> sdpPartTaskLink      bound boundFiles) $
  TaskNode (pure . mkPartChunkTask <$> sdpPartChunkTaskLink bound boundFiles) $
  tSigEpsTaskChain stats bound boundFiles
  where
    mkPartTask      = mkTaskWithStats toPartStatKey      stats.partStats
    mkPartChunkTask = mkTaskWithStats toPartChunkStatKey stats.partChunkStats

tSigEpsTaskChain
  :: (HasTSigEpsDependencies b, MonadPathExists m)
  => CollectedStats
  -> Bound (Precision p) b
  -> BoundFiles
  -> TaskChain m (SumDropVoid (SDPFetchKeys b)) (Block3dGrouper Task)
tSigEpsTaskChain stats bound boundFiles =
  TaskMerge b3dTree $
  TaskMerge
  (TaskNode (pure . mkTTOTask     <$> compositeBlockTaskLink boundFiles.blockDir) b3dTree)
  (TaskNode (pure . mkTSMixedTask <$> compositeBlockTaskLink boundFiles.blockDir) b3dTree)
  where
    mkB3dTask     = mkTaskWithStats toBlock3dStatKey        stats.block3dStats
    mkTTOTask     = mkTaskWithStats toCompositeBlockStatKey stats.ttoBlockStats
    mkTSMixedTask = mkTaskWithStats toCompositeBlockStatKey stats.tsMixedBlockStats
    b3dTree =
      TaskNode (fmap mkB3dTask <$> block3dTaskLink bound.boundConfig boundFiles)
      TaskNil

boundTaskTree
  :: (CanBuildBound p b, MonadPathExists m)
  => CollectedStats
  -> Bound (Precision p) b
  -> BoundFiles
  -> m (Tree Task)
boundTaskTree stats bound boundFiles = do
  maybeTree <- toTaskTree (boundTaskChain stats bound boundFiles) ()
  pure .
    runBlock3dGrouper . sequenceA .
    fromMaybe (error "Empty task tree") $
    maybeTree

data FilesystemModel = PureFilesystem (OsPath -> Bool) | RealFilesystem

buildBoundsSimulated
  :: CanBuildBound p b
  => Int
  -> NominalDiffTime
  -> CollectedStats
  -> FilesystemModel
  -> [Node]
  -> NonEmpty (Bound (Precision p) b, BoundFiles)
  -> IO (TaskStats (DummyTask Task))
buildBoundsSimulated speedup reportInterval stats filesystemModel nodes boundsWithFiles =
  runJobLocal' $ do
  trees <- case filesystemModel of
    PureFilesystem pathExistsFn -> pure $
      runPurePathExists pathExistsFn $
      forM boundsWithFiles $ \(bound, boundFiles) ->
      boundTaskTree stats bound boundFiles
    RealFilesystem ->
      runMemoizedPathExists $
      forM boundsWithFiles $ \(bound, boundFiles) ->
      boundTaskTree stats bound boundFiles
  let taskTree = fmap (MkDummyTask speedup) $ Tree.Node (mkTask ()) (NonEmpty.toList trees)
  runTasksOnNodes reportInterval nodes taskTree

-- | Build the given bounds, write and return a new CollectedStats file.
buildBounds
  :: forall p b . CanBuildBound p b
  => NominalDiffTime
  -> [FilePath]
  -> NonEmpty (Bound (Precision p) b, BoundFiles)
  -> Job FilePath
buildBounds reportInterval statFiles boundsWithFiles = do
  oldCollectedStats <- readCollectedStats statFiles
  -- Use a memoized version of 'doesFileExist', that we don't query
  -- the filesystem so much.
  trees <- runMemoizedPathExists $
    forM boundsWithFiles $ \(bound, boundFiles) ->
    boundTaskTree oldCollectedStats bound boundFiles
  newTaskStats <- runTasks reportInterval $ Tree.Node (mkTask ()) (NonEmpty.toList trees)
  let
    bounds = fmap fst boundsWithFiles
    boundFiles1 = snd (NonEmpty.head boundsWithFiles)
    statsDir = replaceBaseName (dropTrailingPathSeparator boundFiles1.jsonDir) "stats"
    rawStatsFile               = statsDir </> hashBasePath "task_stats_raw"           bounds <.> "json"
    newCollectedStatsFile      = statsDir </> hashBasePath "collected_stats_new"      bounds <.> "json"
    combinedCollectedStatsFile = statsDir </> hashBasePath "collected_stats_combined" bounds <.> "json"
    newCollectedStats  = collectTSigEpsTaskStats (Proxy @p) newTaskStats
  Log.info "Writing raw task stats to file" rawStatsFile
  encodeJsonFileAtomic rawStatsFile newTaskStats
  writeCollectedStats newCollectedStatsFile newCollectedStats
  writeCollectedStats combinedCollectedStatsFile (newCollectedStats <> oldCollectedStats)
  pure combinedCollectedStatsFile

boundPartFiles :: (ToSDP b, KnownNat p) => Bound (Precision p) b -> BoundFiles -> [FilePath]
boundPartFiles bound boundFiles =
  map (SDPB.partPath boundFiles.jsonDir) (SDPB.parts (boundToSDPDeps bound))

boundSDPBInput :: (ToSDP b, KnownNat p) => Bound (Precision p) b -> BoundFiles -> SDPB.Input
boundSDPBInput bound boundFiles =
  sdpbInputFromBoundFiles boundFiles (boundPartFiles bound boundFiles) bound.solverParams

-- | Run pvm2sdp only for the given Bound, without running SDPB.
pmp2sdpBound :: (ToSDP b, KnownNat p, MonadIO m) => Bound (Precision p) b -> BoundFiles -> m ()
pmp2sdpBound bound boundFiles = liftIO $ SDPB.runPmp2sdp
  (bound.boundConfig.scriptsDir </> "srun_pmp2sdp.sh")
  (boundSDPBInput bound boundFiles)

-- | Run SDPB to compute the given bound. This function assumes that
-- the bound pmp files have already been built.
runSDPBBound :: (ToSDP b, KnownNat p, MonadIO m) => Bound (Precision p) b -> BoundFiles -> m SDPB.Output
runSDPBBound bound boundFiles = liftIO $ SDPB.run
  (scriptsDir bound.boundConfig </> "srun_pmp2sdp.sh")
  (scriptsDir bound.boundConfig </> "srun_sdpb.sh")
  (boundSDPBInput bound boundFiles)

-- TODO: Right now we are specializing to TSigEps3d because we don't
-- have Static versions of (HasTSigEpsDependencies b). In the future,
-- we want a class related to the TaskChain, which will allow us to
-- make these functions polymorphic in the bound.

computeBound
  :: Dict (KnownNat p)
  -> NominalDiffTime
  -> [FilePath]
  -> BoundFileTreatment
  -> Maybe NominalDiffTime
  -> Maybe Int
  -> Bound (Precision p) TSigEps3d
  -> BoundFiles
  -> Job (SDPB.Output, FilePath)
computeBound Dict reportInterval statFiles fileTreatment maybeJobTime centeringIterations bound boundFiles' = do
  -- Ask SDPB to terminate within 90% of the jobTime. This should
  -- ensure a sufficiently prompt exit as long as a single iteration
  -- doesn't take >=10% of the jobTime.
  solverParamsLimitedTime <- case maybeJobTime of
    Nothing      -> pure bound.solverParams
    Just jobTime -> liftIO $ setFixedTimeLimit (0.9*jobTime) bound.solverParams
  let boundFixedTimeLimit = bound { solverParams = solverParamsLimitedTime }

  -- If checkpointDir already exists, use it (overriding initialCheckpointDir).
  checkpointExists <- liftIO $ doesPathExist boundFiles'.checkpointDir
  let boundFiles =
        if checkpointExists
        then boundFiles' { initialCheckpointDir = Nothing }
        else boundFiles'

  -- Build the sdp and its dependencies
  Log.info "Building sdp" (boundFixedTimeLimit, boundFiles)
  newStatFile <- buildBounds reportInterval statFiles $
    NonEmpty.singleton (boundFixedTimeLimit, boundFiles)

  -- Run SDPB
  initialOutput <- runSDPBBound boundFixedTimeLimit boundFiles
  Log.info "SDPB result" (boundFixedTimeLimit, initialOutput)

  -- Possibly perform centering iterations
  output <- case centeringIterations of
    Just n
      | SDPB.isFinished initialOutput -> do
          Log.info "Performing centering iterations" n
          let
            centeringBound = modifySolverParams (setCenteringIterations n) boundFixedTimeLimit
            -- ignore initialCheckpointDir, so we start from where we
            -- left off
            centeringFiles = boundFiles { initialCheckpointDir = Nothing}
          centeringOutput <- runSDPBBound centeringBound centeringFiles
          -- Modify the output to have the same terminateReason as the
          -- initial run. This is necessary for runSDPBUntilFinished
          -- to work correctly.
          let output' = centeringOutput { SDPB.terminateReason = initialOutput.terminateReason }
          Log.info "SDPB result (centering)" output'
          pure output'
    _ -> pure initialOutput

  -- If finished, store the result in the database, associated to the
  -- original bound -- not the one with modified time limit or
  -- centering iterations. NB1: This means the DB will not be aware of
  -- the centering iterations. NB2: we store to the database from the
  -- Job, in case the master gets killed while the Job is running.
  when (SDPB.isFinished output) $ do
    DB.insert computationsKeyValMap bound output
    cleanFilesWithTreatment fileTreatment boundFiles
  return (output, newStatFile)

modifySolverParams :: (SDPB.Params -> SDPB.Params) -> Bound p b -> Bound p b
modifySolverParams f bound = bound { solverParams = f bound.solverParams }

setCenteringIterations :: Int -> SDPB.Params -> SDPB.Params
setCenteringIterations n params = params
  { SDPB.maxIterations                = n
  , SDPB.stepLengthReduction          = 1
  , SDPB.infeasibleCenteringParameter = 1
  , SDPB.dualityGapThreshold          = 0
  , SDPB.primalErrorThreshold         = 0
  , SDPB.dualErrorThreshold           = 0
  }

setDualityGap :: Scientific -> SDPB.Params -> SDPB.Params
setDualityGap gap params = params { SDPB.dualityGapThreshold = gap }

-- | Write x and y so that we can later run spectrum (specifically x
-- is needed for spectrum).
setWriteSpectrumData :: SDPB.Params -> SDPB.Params
setWriteSpectrumData params = params
  { SDPB.writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.Functional_z, SDPB.PrimalVector_x] }

computationsKeyValMap :: DB.KeyValMap (Bound (Precision p) TSigEps3d) SDPB.Output
computationsKeyValMap = DB.KeyValMap "computations"

-- | Repeatedly run SDPB until the result satisfies isFinished.
runSDPBUntilFinished :: MonadIO m => m SDPB.Output -> m SDPB.Output
runSDPBUntilFinished run = go
  where
    go = do
      result <- run
      if SDPB.isUnfinished result
        then do
        Log.info "SDPB result is unfinished. Running again." result
        go
        else
        pure result

remoteComputeBound
  :: KnownNat p
  => NominalDiffTime
  -> [FilePath]
  -> BoundFileTreatment
  -> Maybe Int
  -> Bound (Precision p) TSigEps3d
  -> (FilePath -> BoundFiles)
  -> Cluster (SDPB.Output, BoundFiles)
remoteComputeBound reportInterval statFiles fileTreatment centeringIterations bound mkBoundFiles = do
  workDir <- newWorkDir bound
  jobTime <- asks (Slurm.time . clusterJobOptions)
  let
    boundFiles = mkBoundFiles workDir
    run = do
      (result, _) <- remoteEvalJob $
        static computeBound
        `cAp` closureDict
        `cAp` cPure reportInterval
        `cAp` cPure statFiles
        `cAp` cPure fileTreatment
        `cAp` cPure (Just jobTime)
        `cAp` cPure centeringIterations
        `cAp` cPure bound
        `cAp` cPure boundFiles
      Log.info "Computed" (bound, boundFiles, result)
      pure result
  result <- runSDPBUntilFinished run
  return (result, boundFiles)
