{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.TSigEpsStats where

import Blocks.Blocks3d                              qualified as B3d
import Control.DeepSeq                              (NFData, deepseq)
import Control.Monad.IO.Class                       (MonadIO, liftIO)
import Data.Aeson                                   (FromJSON, ToJSON)
import Data.Aeson                                   qualified as Aeson
import Data.Map.Strict                              (Map)
import Data.Map.Strict                              qualified as Map
import GHC.Generics                                 (Generic)
import GHC.TypeNats                                 (KnownNat)
import Hyperion.Log                                 qualified as Log
import Hyperion.Util                                (randomString)
import StressTensors3d.Scheduler.Block3dTask        (Block3dStatKey,
                                                     Block3dTask (..),
                                                     toBlock3dStatKey)
import StressTensors3d.Scheduler.CompositeBlockTask (CompositeBlockStatKey,
                                                     CompositeBlockTask (..),
                                                     toCompositeBlockStatKey)
import StressTensors3d.Scheduler.PartChunkTask      (PartChunkStatKey,
                                                     PartChunkTask (..),
                                                     toPartChunkStatKey)
import StressTensors3d.Scheduler.PartTask           (PartStatKey, PartTask (..),
                                                     toPartStatKey)
import StressTensors3d.Scheduler.RunTasks           (TaskStat (..))
import StressTensors3d.Scheduler.Stats              (TaskResourceMap,
                                                     statsToResourceMap)
import StressTensors3d.Scheduler.Task               (Task, matchTask)
import StressTensors3d.TSigEps3d                    (TSigEps3d (..))
import StressTensors3d.TSigEpsSetup                 (TSMixedStructLabel)
import StressTensors3d.TTOStructLabel               (TTOStructLabel)
import System.Directory                             (createDirectoryIfMissing,
                                                     renameFile)
import System.FilePath.Posix                        (takeDirectory)

data CollectedStats = MkCollectedStats
  { block3dStats      :: Map Block3dStatKey                             TaskResourceMap
  , ttoBlockStats     :: Map (CompositeBlockStatKey TTOStructLabel)     TaskResourceMap
  , tsMixedBlockStats :: Map (CompositeBlockStatKey TSMixedStructLabel) TaskResourceMap
  , partStats         :: Map (PartStatKey B3d.Block3dParams)            TaskResourceMap
  , partChunkStats    :: Map (PartChunkStatKey B3d.Block3dParams)       TaskResourceMap
  } deriving (Eq, Ord, Show, Generic, FromJSON, ToJSON, NFData)

instance Semigroup CollectedStats where
  MkCollectedStats a1 b1 c1 d1 e1 <> MkCollectedStats a2 b2 c2 d2 e2 =
    MkCollectedStats
    (Map.unionWith (<>) a1 a2)
    (Map.unionWith (<>) b1 b2)
    (Map.unionWith (<>) c1 c2)
    (Map.unionWith (<>) d1 d2)
    (Map.unionWith (<>) e1 e2)

instance Monoid CollectedStats where
  mempty = MkCollectedStats mempty mempty mempty mempty mempty

collectTSigEpsTaskStats :: forall p proxy . KnownNat p => proxy p -> [TaskStat Task] -> CollectedStats
collectTSigEpsTaskStats _ = foldMap matchTaskStats
  where
    matchTaskStats stats
      | Just t <- matchTask @Block3dTask stats.task = mempty
        { block3dStats      = Map.singleton (toBlock3dStatKey t)        (statsToResourceMap stats) }
      | Just t <- matchTask @(CompositeBlockTask TTOStructLabel) stats.task = mempty
        { ttoBlockStats     = Map.singleton (toCompositeBlockStatKey t) (statsToResourceMap stats) }
      | Just t <- matchTask @(CompositeBlockTask TSMixedStructLabel) stats.task = mempty
        { tsMixedBlockStats = Map.singleton (toCompositeBlockStatKey t) (statsToResourceMap stats) }
      | Just t <- matchTask @(PartTask p TSigEps3d) stats.task = mempty
        { partStats         = Map.singleton (toPartStatKey t)           (statsToResourceMap stats) }
      | Just t <- matchTask @(PartChunkTask p TSigEps3d) stats.task = mempty
        { partChunkStats    = Map.singleton (toPartChunkStatKey t)      (statsToResourceMap stats) }
      | otherwise =  mempty

writeCollectedStats :: MonadIO m => FilePath -> CollectedStats -> m ()
writeCollectedStats statFile stats = do
  Log.info "Writing CollectedStats to file" statFile
  encodeJsonFileAtomic statFile stats

readCollectedStats :: MonadIO m => [FilePath] -> m CollectedStats
readCollectedStats statFiles = go statFiles mempty
  where
    go [] acc = pure acc
    go (file : files) acc = do
      newStats_either <- liftIO $ Aeson.eitherDecodeFileStrict file
      newStats <- case newStats_either of
        Left e  -> Log.warn "Couldn't parse stats file" (file, e) >> pure mempty
        Right s -> pure s
      newStats `deepseq` go files (acc <> newStats)

-- | Write a json representation of 'x' to a temporary file, and then
-- move the temporary file into place.
--
-- TODO: This is a copy of a function in SDPB. Export the function or
-- relocate it.
encodeJsonFileAtomic :: (MonadIO m, ToJSON a) => FilePath -> a -> m ()
encodeJsonFileAtomic path x = liftIO $ do
  salt <- randomString 6
  let tmpPath = path <> "_" <> salt
  createDirectoryIfMissing True (takeDirectory path)
  Aeson.encodeFile tmpPath x
  renameFile tmpPath path
