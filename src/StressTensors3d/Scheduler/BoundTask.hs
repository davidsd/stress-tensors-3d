{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Scheduler.BoundTask where

import Data.Aeson                         (ToJSON)
import Data.Binary                        (Binary)
import Data.Set                           qualified as Set
import GHC.Generics                       (Generic)
import GHC.TypeNats                       (KnownNat)
import Hyperion.Bootstrap.Bound           (Bound (..), BoundFiles (..),
                                           Precision (..), ToSDP,
                                           boundToSDPDeps, reflectBound)
import SDPB qualified
import StressTensors3d.Scheduler.RunTasks (CanRemoteRunTask (..))
import StressTensors3d.Scheduler.Task     (HasTaskHash (..))
import StressTensors3d.Scheduler.TaskInfo (HasTaskInfo (..), TaskInfo (..))
import StressTensors3d.Scheduler.TaskLink (TaskLink (..))

-- | A "Dummy" task whose purpose is to be a unique node in the tree,
-- so that we can combine trees for different bounds.
data BoundTask b = MkBoundTask
  { bound      :: Bound Int b
  , boundFiles :: BoundFiles
  } deriving (Generic, Binary, ToJSON, HasTaskHash)

instance HasTaskInfo (BoundTask b) where
  taskInfo _ = MkTaskInfo
    { memory     = 0
    , runtime    = const 0
    , maxThreads = const 0
    , minThreads = const 0
    , tag        = Just "Bound"
    }

instance CanRemoteRunTask (BoundTask b) where
  remoteRunTask _ _ _ = pure (Just 0)

boundTaskLink
  :: (Applicative m, KnownNat p, ToSDP b)
  => Bound (Precision p) b
  -> BoundFiles
  -> TaskLink m () SDPB.Part (BoundTask b)
boundTaskLink bound boundFiles = MkTaskLink
  { dependencies = const (Set.fromList (SDPB.parts (boundToSDPDeps bound)))
  , checkCreated = const (pure False)
  , toTask       = const (MkBoundTask (reflectBound bound) boundFiles)
  }
