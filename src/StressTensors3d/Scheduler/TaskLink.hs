{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE NoFieldSelectors      #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE UndecidableInstances  #-}

module StressTensors3d.Scheduler.TaskLink where

import Bootstrap.Build         (BuildLink (..))
import Data.Map.Strict         (Map)
import Data.Map.Strict         qualified as Map
import Data.Maybe              (catMaybes)
import Data.Set                (Set)
import Data.Set                qualified as Set
import Data.Tree               (Tree)
import Data.Tree               qualified as Tree
import Data.Void               (Void)

data TaskLink m k d t = MkTaskLink
  { dependencies :: k -> Set d
  , checkCreated :: k -> m Bool
  , toTask       :: k -> t
  } deriving (Functor)

data TaskChain m k t where
  TaskNil   :: TaskChain m Void t
  TaskNode  :: TaskLink m k d t -> TaskChain m d t -> TaskChain m k t
  TaskMerge :: TaskChain m k1 t -> TaskChain m k2 t -> TaskChain m (Either k1 k2) t

-- | Turn a TaskChain into a tree of tasks
toTaskTree :: Monad m => TaskChain m k t -> k -> m (Maybe (Tree t))
toTaskTree TaskNil _ = error "absurd"
toTaskTree (TaskNode link chain) key = do
  created <- link.checkCreated key
  if created
    then pure Nothing
    else do
    deps <- mapM (toTaskTree chain) (Set.toList (link.dependencies key))
    pure $ Just $ Tree.Node (link.toTask key) (catMaybes deps)
toTaskTree (TaskMerge c1 c2) key = case key of
  Left k1  -> toTaskTree c1 k1
  Right k2 -> toTaskTree c2 k2

-- | Given a Tree, tabulate the pairs of (node, children). This
-- function assumes that a given node always has the same children, no
-- matter where or how many times it appears in the tree. If this is
-- not the case, then only one of the instances of the node will be
-- used.
treeToEdges :: Ord a => Tree a -> Map a (Set a)
treeToEdges t = go t Map.empty
  where
    go (Tree.Node x xs) m =
      Map.insert x (Set.fromList (map Tree.rootLabel xs)) $
      foldr go m xs

buildLinkToTaskLink
  :: Ord d
  => (k -> t)
  -> BuildLink m k d
  -> TaskLink m k d t
buildLinkToTaskLink toTask link = MkTaskLink
  { dependencies = Set.fromList . link.buildDeps
  , checkCreated = link.checkCreated
  , toTask = toTask
  }
