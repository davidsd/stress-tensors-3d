{-# LANGUAGE DeriveAnyClass #-}

module StressTensors3d.Z2Rep
  ( Z2Rep(..)
  ) where

import Control.DeepSeq (NFData)
import Data.Aeson      (FromJSON, ToJSON)
import Data.Binary     (Binary)
import GHC.Generics    (Generic)

data Z2Rep = Z2Even | Z2Odd
  deriving (Show, Eq, Ord, Enum, Bounded, Generic, Binary, ToJSON, FromJSON, NFData)
