{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.TSigEpsOPEScan where

import Blocks.Blocks3d                      qualified as B3d
import Bootstrap.Bounds                     (BoundDirection)
import Bootstrap.Bounds.Spectrum            qualified as Spectrum
import Bootstrap.Math.AffineTransform       (AffineTransform)
import Bootstrap.Math.HalfInteger           (HalfInteger)
import Bootstrap.Math.VectorSpace           ((*^))
import Control.Monad                        (void)
import Control.Monad.IO.Class               (liftIO)
import Data.Scientific                      (Scientific)
import Data.Time                            (NominalDiffTime)
import Hyperion                             (Cluster, doConcurrently_,
                                             forConcurrently_, mapConcurrently_)
import Hyperion.Bootstrap.Bound             (Bound (..), LambdaMap)
import Hyperion.Bootstrap.Bound             qualified as Bound
import Hyperion.Bootstrap.DelaunaySearch    (DelaunayConfig,
                                             delaunaySearchRegionPersistent)
import Hyperion.Bootstrap.Params            qualified as Params
import Hyperion.Database                    qualified as DB
import Hyperion.Log                         qualified as Log
import Hyperion.TokenPool                   qualified as TokenPool
import Hyperion.Util                        (minute)
import Linear.V                             (V)
import SDPB qualified
import StressTensors3d.OPEScan              qualified as OPEScan
import StressTensors3d.Programs.Defaults    (defaultBoundConfig,
                                             defaultQuadraticNetConfig)
import StressTensors3d.Programs.TSig2023    (z2EvenParityEvenScalar,
                                             z2EvenParityOddScalar,
                                             z2OddParityEvenScalar)
import StressTensors3d.Programs.TSigEps2023 (boundDeltaV, delaunaySearchPoints,
                                             deltaVToExternalDims, mkPointMap,
                                             z2EvenParityEvenSpin2)
import StressTensors3d.Scheduler.TSigEps    (modifySolverParams, setDualityGap,
                                             setWriteSpectrumData)
import StressTensors3d.Scheduler.TSigEps    qualified as Scheduler
import StressTensors3d.TSigEps3d            (Objective (..), TSigEps3d (..))
import StressTensors3d.TSigEps3d            qualified as TSigEps3d
import StressTensors3d.TSigEpsSetup         (SymmetrySector)

objectiveParams :: TSigEps3d.Objective -> Int -> SDPB.Params
objectiveParams = \case
  Feasibility _ -> Params.jumpFindingParams
  _             -> Params.optimizationParams

spectrumDefaultGaps :: Spectrum.Spectrum SymmetrySector
spectrumDefaultGaps = Spectrum.setGaps
  [ (z2EvenParityEvenScalar, 3 )
  , (z2OddParityEvenScalar, 3 )
  , (z2EvenParityOddScalar, 3 )
  , (z2EvenParityEvenSpin2, 4)
  ]
  $ Spectrum.setTwistGap 1e-6 $ Spectrum.unitarySpectrum

data TSigEpsParams = MkTSigEpsParams
  { maxSpin   :: HalfInteger
  , order     :: Int
  , numPoles  :: Int
  , precision :: Int
  , nmax      :: Int
  } deriving (Eq, Ord, Show)

tSigEpsBound :: TSigEpsParams -> V 2 Rational -> Objective -> Bound Int TSigEps3d
tSigEpsBound tSigEpsParams deltaExt objective = Bound
  { boundKey = TSigEps3d
    { spectrum = spectrumDefaultGaps
    , objective   = objective
    , blockParams = B3d.Block3dParams
      { -- turn off pole shifting by setting keptPoleOrder = order
        B3d.keptPoleOrder = tSigEpsParams.order
      , B3d.order         = tSigEpsParams.order
      , B3d.precision     = tSigEpsParams.precision
      , B3d.nmax          = tSigEpsParams.nmax
      }
    , numPoles     = Just tSigEpsParams.numPoles
    , spins        = [0 .. tSigEpsParams.maxSpin]
    , externalDims = deltaVToExternalDims deltaExt
    }
  , precision = tSigEpsParams.precision
  , solverParams = (objectiveParams objective tSigEpsParams.nmax)
    { SDPB.precision                = tSigEpsParams.precision
    , SDPB.initialMatrixScaleDual   = 1e20
    , SDPB.initialMatrixScalePrimal = 1e20
    }
  , boundConfig = defaultBoundConfig
  }

tSigEpsFeasible :: TSigEpsParams -> V 2 Rational -> Maybe (V 5 Rational) -> Bound Int TSigEps3d
tSigEpsFeasible tSigEpsParams deltaExt maybeLambda =
  tSigEpsBound tSigEpsParams deltaExt (Feasibility maybeLambda)

tSigEpsOPEBound :: TSigEpsParams -> BoundDirection -> (V 2 Rational, V 5 Rational) -> Bound Int TSigEps3d
tSigEpsOPEBound tSigEpsParams boundDirection (deltaExt, lambda) =
  tSigEpsBound tSigEpsParams deltaExt (ExternalOPEBound lambda boundDirection)

remoteTSigEps3dOPEScan
  :: Bound.CheckpointMap TSigEps3d
  -> LambdaMap TSigEps3d (V 5 Rational)
  -> [FilePath]
  -> OPEScan.BilinearForms 5
  -> Bound Int TSigEps3d
  -> Cluster Bool
remoteTSigEps3dOPEScan checkpointMap lambdaMap statFiles initialBilinearForms =
  OPEScan.remoteOPESearch (static opeSearchCfg) checkpointMap lambdaMap statFiles initialBilinearForms
  where
    opeSearchCfg = OPEScan.OPESearchConfig setLambda TSigEps3d.getMatExt $
      OPEScan.queryAllowedMixed qmConfig
    setLambda lambda bound = bound
      { boundKey = bound.boundKey { objective = Feasibility (Just lambda) }
      }
    qmConfig = OPEScan.QueryMixedConfig
      { OPEScan.qmQuadraticNetConfig = defaultQuadraticNetConfig
      , OPEScan.qmDescentConfig      = OPEScan.defaultDescentConfig
        { OPEScan.maxDescentRuns = 10 }
      , OPEScan.qmResolution         = 1e-32
      , OPEScan.qmPrecision          = 384
      , OPEScan.qmHessianLineSteps   = 200
      , OPEScan.qmHessianLineAverage = 10
      }

data TSigEpsOPEScanConfig = MkTSigEpsOPEScanConfig
  { initialBilinearForms :: OPEScan.BilinearForms 5
  , initialLambda        :: Maybe (V 5 Rational)
  , affine               :: AffineTransform 2 Rational
  , params               :: TSigEpsParams
  , initialCheckpoint    :: Maybe FilePath
  , statFiles            :: [FilePath]
  , maxSimultaneousJobs  :: Maybe Int
  }

tSigEpsOPEScan :: TSigEpsOPEScanConfig -> [V 2 Rational] -> Cluster ()
tSigEpsOPEScan cfg deltaExts = do
  checkpointMap <- Bound.newCheckpointMap cfg.affine boundDeltaV cfg.initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     cfg.affine boundDeltaV cfg.initialLambda
  tokenPool     <- liftIO $ TokenPool.newTokenPool cfg.maxSimultaneousJobs
  forConcurrently_ deltaExts $ \deltaExt ->
    TokenPool.withToken tokenPool $
    remoteTSigEps3dOPEScan checkpointMap lambdaMap cfg.statFiles cfg.initialBilinearForms $
    bound deltaExt
  where
    bound deltaExt = tSigEpsFeasible cfg.params deltaExt cfg.initialLambda

data TSigEpsDelaunayConfig = MkTSigEpsDelaunayConfig
  { initialAllowedPoints    :: [V 2 Rational]
  , initialDisallowedPoints :: [V 2 Rational]
  , initialTestPoints       :: [V 2 Rational]
  , delaunayConfig          :: DelaunayConfig
  , opeScanConfig           :: TSigEpsOPEScanConfig
  }

tSigEpsDelaunaySearch :: TSigEpsDelaunayConfig -> Cluster ()
tSigEpsDelaunaySearch cfg = do
  checkpointMap <- Bound.newCheckpointMap opeCfg.affine boundDeltaV opeCfg.initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     opeCfg.affine boundDeltaV opeCfg.initialLambda
  void $ delaunaySearchRegionPersistent delaunaySearchPoints cfg.delaunayConfig opeCfg.affine initialPoints
    (remoteTSigEps3dOPEScan checkpointMap lambdaMap opeCfg.statFiles opeCfg.initialBilinearForms . bound)
  where
    opeCfg = cfg.opeScanConfig
    initialPoints = mkPointMap
      cfg.initialAllowedPoints
      cfg.initialDisallowedPoints
      cfg.initialTestPoints
    bound deltaExt = tSigEpsFeasible opeCfg.params deltaExt opeCfg.initialLambda

data OpeMinMax v b = MkOpeMinMax
  { opeBound                 :: BoundDirection -> v -> Bound Int b
  , maxSimultaneousJobs      :: Maybe Int
  , directionsAndCheckpoints :: [(BoundDirection, Maybe FilePath)]
  , statFiles                :: [FilePath]
  , modifyParams             :: SDPB.Params -> SDPB.Params
  , modifyBoundFiles         :: Bound.BoundFiles -> Bound.BoundFiles
  , centeringIterations      :: Maybe Int
  , allowedPoints            :: [v]
  , dualityGap               :: Scientific
  }

lookupOrComputeBound
  :: NominalDiffTime
  -> [FilePath]
  -> Bound.BoundFileTreatment
  -> Maybe Int
  -> Bound Int TSigEps3d
  -> (FilePath -> Bound.BoundFiles)
  -> Cluster ()
lookupOrComputeBound reportInterval statFiles fileTreatment centeringIterations bound boundFiles =
  Bound.reifyBound bound $ \bound' -> do
  maybeResult <- DB.lookup Scheduler.computationsKeyValMap bound'
  case maybeResult of
    Just result -> Log.info "Already computed" (bound, result)
    Nothing ->
      void $
      Scheduler.remoteComputeBound reportInterval statFiles fileTreatment centeringIterations bound' boundFiles

runOpeMinMax :: OpeMinMax v TSigEps3d -> Cluster ()
runOpeMinMax cfg = do
  tokenPool <- liftIO $ TokenPool.newTokenPool cfg.maxSimultaneousJobs
  doConcurrently_ $ do
    point <- cfg.allowedPoints
    (boundDirection, initialCheckpoint) <- cfg.directionsAndCheckpoints
    let
      bound =
        modifySolverParams (cfg.modifyParams . setDualityGap cfg.dualityGap . setWriteSpectrumData) $
        cfg.opeBound boundDirection point
      boundFiles workDir =
        cfg.modifyBoundFiles $
        (Bound.defaultBoundFiles workDir)
        { Bound.initialCheckpointDir = initialCheckpoint }
    pure $
      TokenPool.withToken tokenPool $
      lookupOrComputeBound (1*minute) cfg.statFiles Bound.keepAllFiles cfg.centeringIterations bound boundFiles

-- Checks the input false point but not the input true point
tSigEpsIntervalBinarySearch
  :: TSigEpsOPEScanConfig
  -> Bound.CheckpointMap TSigEps3d
  -> LambdaMap TSigEps3d (V 5 Rational)
  -> Int
  -> (V 2 Rational, V 2 Rational)
  -> Cluster (Rational, Rational)
tSigEpsIntervalBinarySearch opeCfg checkpointMap lambdaMap bisections (pTrue, pFalse) = do
  outerResult <- testT 1
  if outerResult then return (0,1) else
    bisect bisections (0,1)
  where
    testT :: Rational -> Cluster Bool
    testT t = remoteTSigEps3dOPEScan checkpointMap lambdaMap opeCfg.statFiles opeCfg.initialBilinearForms bound
      where
        bound = tSigEpsFeasible opeCfg.params ((1-t)*^pTrue+t*^pFalse) opeCfg.initialLambda
    bisect :: Int -> (Rational, Rational) -> Cluster (Rational, Rational)
    bisect 0 b = return b
    bisect n (tTrue, tFalse) = do
      let mid = (tTrue+tFalse)/2
      result <- testT mid
      if result then bisect (n-1) (mid, tFalse) else bisect (n-1) (tTrue, mid)

data TSigEpsStarSearchConfig = TSigEpsStarSearchConfig
  { opeScanConfig       :: TSigEpsOPEScanConfig
  , bisections          :: Int
  , intervals           :: [(V 2 Rational, V 2 Rational)]
  , maxSimultaneousJobs :: Maybe Int
  }

runStarSearch :: TSigEpsStarSearchConfig -> Cluster ()
runStarSearch cfg = do
  tokenPool <- liftIO $ TokenPool.newTokenPool cfg.maxSimultaneousJobs
  checkpointMap <- Bound.newCheckpointMap opeCfg.affine boundDeltaV opeCfg.initialCheckpoint
  lambdaMap     <- Bound.newLambdaMap     opeCfg.affine boundDeltaV opeCfg.initialLambda
  mapConcurrently_ (go checkpointMap lambdaMap tokenPool) cfg.intervals
  where
    opeCfg = cfg.opeScanConfig
    go cMap lMap pool b = TokenPool.withToken pool $ tSigEpsIntervalBinarySearch opeCfg cMap lMap cfg.bisections b
