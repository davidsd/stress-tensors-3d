{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.OPEScanNmax18 where

import Bootstrap.Bounds                           (BoundDirection (..))
import Bootstrap.Math.Linear                      (toV)
import Control.Monad.IO.Class                     (liftIO)
import Control.Monad.Reader                       (local)
import Data.Text                                  (Text)
import Hyperion                                   (Cluster, MPIJob (..),
                                                   doConcurrently_,
                                                   setJobMemory, setJobTime,
                                                   setJobType)
import Hyperion.Bootstrap.Bound                   qualified as Bound
import Hyperion.Bootstrap.Main                    (unknownProgram)
import Hyperion.TokenPool                         qualified as TokenPool
import Hyperion.Util                              (hour, minute)
import Linear.V                                   (V)
import StressTensors3d.OPEScan                    qualified as OPEScan
import StressTensors3d.Programs.Defaults          (defaultDelaunayConfig)
import StressTensors3d.Programs.OPEScanNmax18Data (allowedPointsNmax18,
                                                   allowedPointsNmax18SpacedOut,
                                                   disallowedPointsNmax18,
                                                   isingTSigEpsAffineNmax18)
import StressTensors3d.Programs.TSigEps2023Data   (isingSigEpsAffineNmax22,
                                                   opeEllipseNmax14)
import StressTensors3d.Programs.TSigEpsOPEScan    (TSigEpsParams (..),
                                                   lookupOrComputeBound,
                                                   TSigEpsDelaunayConfig(..),
                                                   TSigEpsOPEScanConfig(..),
                                                   tSigEpsDelaunaySearch,
                                                   tSigEpsOPEScan,
                                                   tSigEpsOPEBound)
import StressTensors3d.Scheduler.TSigEps          (modifySolverParams,
                                                   setDualityGap,
                                                   setWriteSpectrumData)

paramsNmax18 :: TSigEpsParams
paramsNmax18 = MkTSigEpsParams
  { maxSpin   = 75
  , order     = 106
  , numPoles  = 35
  , precision = 832
  , nmax      = 18
  }

initialBilinearFormsNmax18 :: OPEScan.BilinearForms 5
initialBilinearFormsNmax18 = OPEScan.BilinearForms 1e-32 [(Nothing, opeEllipseNmax14)]

testOPEScanConfigNmax18 :: Maybe (V 5 Rational) -> Maybe FilePath -> [FilePath] -> TSigEpsOPEScanConfig
testOPEScanConfigNmax18 initialLambda initialCheckpoint statFiles = MkTSigEpsOPEScanConfig
  { initialBilinearForms = initialBilinearFormsNmax18
  , initialLambda        = initialLambda
  , affine               = isingSigEpsAffineNmax22
  , params               = paramsNmax18
  , initialCheckpoint    = initialCheckpoint
  , statFiles            = statFiles
  , maxSimultaneousJobs  = Nothing
  }

tSigEpsDelaunayConfigNmax18 :: [V 2 Rational] -> Maybe FilePath -> [FilePath] -> TSigEpsDelaunayConfig
tSigEpsDelaunayConfigNmax18 initialTestPoints initialCheckpoint statFiles = MkTSigEpsDelaunayConfig
  { opeScanConfig = MkTSigEpsOPEScanConfig
    { initialBilinearForms    = initialBilinearFormsNmax18
    , initialLambda           = Just $ snd $ head allowedPointsNmax18
    , affine                  = isingTSigEpsAffineNmax18
    , params                  = paramsNmax18
    , initialCheckpoint       = initialCheckpoint
    , statFiles               = statFiles
    , maxSimultaneousJobs     = Nothing
    }
  , initialAllowedPoints    = fmap fst allowedPointsNmax18
  , initialDisallowedPoints = disallowedPointsNmax18
  , initialTestPoints       = initialTestPoints
  , delaunayConfig          = defaultDelaunayConfig 6 200
  }

-- nmax=18 OPE upper and lower bounds
opeMinMaxNmax18 :: Maybe Int -> [FilePath] -> Cluster ()
opeMinMaxNmax18 numSimultaneousJobs statFiles = do
  tokenPool <- liftIO $ TokenPool.newTokenPool numSimultaneousJobs
  doConcurrently_ $ do
    (deltaExt, lambda) <- take 1 allowedPointsNmax18SpacedOut
    boundDirection <- [LowerBound, UpperBound]
    let
      bound =
        modifySolverParams (setDualityGap 1e-60 . setWriteSpectrumData) $
        tSigEpsOPEBound paramsNmax18 boundDirection (deltaExt, lambda)
    pure $
      TokenPool.withToken tokenPool $
      lookupOrComputeBound (1*minute) statFiles Bound.keepAllFiles Nothing bound Bound.defaultBoundFiles

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_nmax_18_feasible_point_test" =
  local (setJobType (MPIJob 10 128) . setJobTime (12*hour) . setJobMemory "0G") $
  tSigEpsOPEScan (testOPEScanConfigNmax18 initialLambda initialCheckpoint statFiles) deltaExts
  where
    deltaExts = map toV
      [ --   (0.51814889295469165636644534, 1.4126261269553012059145032) -- disallowed (aaVMQ/0)
        -- , (0.51814873115236157560648932, 1.4126242334383180064957437) -- disallowed (aaVMQ/1)
        -- , (0.5181489478395621367792600, 1.4126269566873575454986824) -- dosallowed (YCPFX/0)
        -- , (0.5181488524299055777439593, 1.4126259515400234279743122) -- disallowed (aaVMQ/3)
        -- , (0.51814873277338427617276519,1.4126244716701922983089676) -- disallowed (aaVMQ/4)
        -- , (0.5181489143060547489662984, 1.4126266615093920379578901) -- disallowed
        -- , (0.5181489519353391776527078, 1.4126268967562636357988424) -- disallowed
        -- , (0.51814881565435278879854877, 1.4126251798983093368087176) -- disallowed
        -- A guess, based on the shape of the nmax=14 island
        (0.51814882338193927145874808, 1.4126254772155267750296378) -- allowed
      ]
    initialLambda = Just $ snd $ head allowedPointsNmax18
      -- This is lambda at the the nmax=14 optimum
      -- Just $ toV
      -- ( 1.06327271904675412467661510507
      -- , 0.0149152903768990937801920979997
      -- , 1.10336466371243625693346602811
      -- , 1.60748815952065469427002084091
      -- , 1
      -- )
    initialCheckpoint =
      Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/ck"
    statFiles =
      ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/json/collected_stats_combined_rY7cVMLgO81TUnhMYgbPRwQtFtss9LIDLlHkbQNGWqw.json"]
      -- [ taskStatsDir </> "TSigEps_test_nmax18_scheduler_expanse_10_128_collected_stats_order_106.json"]


boundsProgram "TSigEps_nmax_18_delaunay_search" =
  local (setJobType (MPIJob 10 128) . setJobTime (12*hour) . setJobMemory "0G") $
  tSigEpsDelaunaySearch $
  tSigEpsDelaunayConfigNmax18 initialTestPoints initialCheckpoint statFiles
  where
    initialCheckpoint =
      Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/ck"
    statFiles =
      ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/json/collected_stats_combined_rY7cVMLgO81TUnhMYgbPRwQtFtss9LIDLlHkbQNGWqw.json"]
    initialTestPoints = map toV
      [ (0.518148734363423707764302823014435490, 1.41262435235525497280420419295190476)
      ]
      -- [ (0.518148768107304750391733538181327068, 1.41262476669890767115114628055830043)
      -- , (0.518148753388439576921976563684333313, 1.41262458914437796253083018239128170)
      -- , (0.518148754993970643000883315889815056, 1.41262464860284644568506043713872379)
      -- , (0.518148746681738099359384229540387178, 1.41262453010878486102267172531899804)
      -- , (0.518148769712835816470640290386808811, 1.41262482615737615430537653530574252)
      -- , (0.518148766740605060452019598326894709, 1.41262480852629829569251811374243558)
      -- , (0.518148781431012512597052804919999982, 1.41262500396065608742338192622285444)
      -- , (0.518148771078311804362106137262684539, 1.41262488563574465541941972412737912)
      -- , (0.518148870539223269211227290809090006, 1.41262600963036567639125411688570849)
      -- , (0.51814887795124926964972303916937711,  1.41262612819905232840294961653046220)
      -- ]

-- nmax=18 OPE upper and lower bounds
boundsProgram "TSigEps_nmax18_ope_bounds" =
  local (setJobType (MPIJob 10 128) . setJobTime (16*hour) . setJobMemory "0G") $
  opeMinMaxNmax18 numSimultaneousJobs statFiles
  where
    statFiles =
      ["/expanse/lustre/scratch/dsd/temp_project/data/2024-07/FJRzL/Object_gnUakbYsrmLtQesXhcIMtRqiPuaSjnKUBpkeUPx-FoY/json/collected_stats_combined_rY7cVMLgO81TUnhMYgbPRwQtFtss9LIDLlHkbQNGWqw.json"]
    numSimultaneousJobs = Just 6

boundsProgram p = unknownProgram p
