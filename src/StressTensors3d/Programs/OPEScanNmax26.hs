{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.OPEScanNmax26 where

import Bootstrap.Math.Linear                      (toV)
import Control.Monad.Reader                       (local)
import Data.Text                                  (Text)
import Hyperion                                   (Cluster, MPIJob (..),
                                                   setJobMemory, setJobTime,
                                                   setJobType)
import Hyperion.Bootstrap.Main                    (unknownProgram)
import Hyperion.Util                              (hour)
import StressTensors3d.OPEScan                    qualified as OPEScan
import StressTensors3d.Programs.Defaults          (defaultDelaunayConfig,
                                                   taskStatsDir)
import StressTensors3d.Programs.OPEScanNmax22Data (disallowedPointsNmax22,
                                                   isingTSigEpsAffineNmax22,
                                                   opeEllipseNmax22)
import StressTensors3d.Programs.OPEScanNmax26Data (allowedPointsNmax26, testIntervalsPKNmax26)
import StressTensors3d.Programs.TSigEpsOPEScan    (TSigEpsDelaunayConfig (..),
                                                   TSigEpsOPEScanConfig (..),
                                                   TSigEpsParams (..),
                                                   TSigEpsStarSearchConfig (..),
                                                   tSigEpsDelaunaySearch,
                                                   tSigEpsOPEScan,
                                                   runStarSearch)
import System.FilePath.Posix                      ((</>))

paramsNmax26 :: TSigEpsParams
paramsNmax26 = MkTSigEpsParams
  { maxSpin   = 100
  , order     = 152
  , numPoles  = 35
  , precision = 896
  , nmax      = 26
  }

initialBilinearFormsNmax26 :: OPEScan.BilinearForms 5
initialBilinearFormsNmax26 = OPEScan.BilinearForms 1e-32 [(Nothing, opeEllipseNmax22)]

opeScanConfigNmax26 :: TSigEpsOPEScanConfig
opeScanConfigNmax26 = MkTSigEpsOPEScanConfig
  { initialBilinearForms = initialBilinearFormsNmax26
  , initialLambda        = Just (snd (head allowedPointsNmax26))
  , affine               = isingTSigEpsAffineNmax22
  , params               = paramsNmax26
  , initialCheckpoint    = initialCheckpointFeasibleNmax26
  , statFiles            = statFilesNmax26
  , maxSimultaneousJobs  = Just 4
  }

starSearchConfigPKNmax26 :: TSigEpsStarSearchConfig
starSearchConfigPKNmax26 = TSigEpsStarSearchConfig
  { opeScanConfig       = opeScanConfigNmax26
  , bisections          = 5
  , intervals           = testIntervalsPKNmax26
  , maxSimultaneousJobs = Nothing
  }

initialCheckpointFeasibleNmax26 :: Maybe FilePath
initialCheckpointFeasibleNmax26 = Just "/expanse/lustre/scratch/dsd/temp_project/data/2024-12/VUNMG/Object_oPB3Txe8gbFLpa2nUfUP09Pq4qTCzbfiwzHi2y0x12s/ck"

statFilesNmax26 :: [FilePath]
statFilesNmax26 = [taskStatsDir </> "TSigEps_nmax_26_feasible_point_test_collected_stats.json"]

tSigEpsDelaunayConfigNmax26 :: TSigEpsDelaunayConfig
tSigEpsDelaunayConfigNmax26 = MkTSigEpsDelaunayConfig
  { opeScanConfig           = opeScanConfigNmax26
  , initialAllowedPoints    = map fst allowedPointsNmax26
  , initialDisallowedPoints = disallowedPointsNmax22
  , initialTestPoints       = []
  , delaunayConfig          = defaultDelaunayConfig 4 200
  }

setJobNmax26 :: Cluster a -> Cluster a
setJobNmax26 = local (setJobType (MPIJob 16 128) . setJobTime (20*hour) . setJobMemory "0G")

setJobPKNmax26 :: Cluster a -> Cluster a
setJobPKNmax26 = local (setJobType (MPIJob 16 128) . setJobTime (30*hour) . setJobMemory "0G")

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_nmax_26_feasible_point_test" =
  setJobNmax26 $
  tSigEpsOPEScan opeScanConfigNmax26 $
  map toV
  [ --   (0.5181487936495730028951812, 1.412625144436968405159405)
    -- , (0.5181487917488842853686037, 1.412625114254747016317992)
    -- , (0.5181487942214675346264130, 1.412625139450295996823570)
    -- , (0.5181488098919593277003059, 1.412625330543229473789779)
    --   (0.5181488155882194579504585, 1.412625405761884334054912)
    -- , (0.5181488166051536614631345, 1.412625424907021587372924)
    -- , (0.5181488096454752723118986, 1.412625343866906746370660)
    -- , (0.5181488015580621997457911, 1.412625245690746655924386)
    --   (0.5181488179160137486078952, 1.412625439756064249507972)
    -- , (0.5181487907092222622296163, 1.412625101196917531964914)
    -- , (0.5181488014053515778201131, 1.412625244539758906370253)
    -- , (0.5181488041728029569910063, 1.412625258204057709932044)
    -- , (0.5181487968132828525469336, 1.412625186159586654355280)
    -- , (0.5181488155997695521648438, 1.412625414306090565474960)
    -- , (0.5181488130946306780444388, 1.412625372849118043916405)
    --   (0.5181488007644250437522260, 1.412625237114076615085878)
    -- , (0.5181488167702651415069681, 1.412625422757521453931417)
    -- , (0.5181487968380305009219455, 1.412625170135485053179991)
    (0.5181488004633463262393889, 1.412625233933754387294357)
  , (0.5181488004229274357825830, 1.412625233936989799232720)
  ]

boundsProgram "TSigEps_nmax_26_PK_star_search" =
  setJobNmax26 $
  runStarSearch starSearchConfigPKNmax26

boundsProgram "TSigEps_nmax_26_delaunay_search" =
  setJobNmax26 $
  tSigEpsDelaunaySearch tSigEpsDelaunayConfigNmax26

boundsProgram p = unknownProgram p
