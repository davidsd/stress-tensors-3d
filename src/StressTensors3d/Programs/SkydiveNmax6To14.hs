{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module StressTensors3d.Programs.SkydiveNmax6To14 where

import Blocks.Blocks3d                      qualified as B3d
import Bootstrap.Math.Linear                (V, toV)
import Bootstrap.Math.Linear                qualified as L
import Control.Monad.Reader                 (local)
import Data.Matrix.Static                   (Matrix)
import Data.Matrix.Static                   qualified as M
import Data.Text                            (Text)
import Hyperion                             (Cluster, Job, MPIJob (..),
                                             newWorkDir, remoteEvalJob,
                                             setJobMemory, setJobTime,
                                             setJobType, setSlurmPartition)
import Hyperion.Bootstrap.Bound             (Bound (..), Precision (..))
import Hyperion.Bootstrap.Bound             qualified as Bound
import Hyperion.Bootstrap.Main              (unknownProgram)
import Hyperion.Bootstrap.Params            (optimizationParams)
import Hyperion.Bootstrap.Params            qualified as Params
import Hyperion.Log                         qualified as Log
import Hyperion.Util                        (hour, minute)
import SDPB                                 (BigFloat, Params (..))
import SDPB                                 qualified as SDPB
import StressTensors3d.Programs.Defaults    (defaultBoundConfig)
import StressTensors3d.Programs.TSigEps2023 (tSigEpsNavigatorDefaultGaps)
import StressTensors3d.Skydive              (SkydiveConfig (..),
                                             defaultSkydiveConfig,
                                             skydiveBoundLoop, SkydiveState(..), defaultExactHessianClock)
import StressTensors3d.TSigEps3d            (TSigEps3d (..))

---------------
-- 1D search --
---------------

testDynamicalSdpStressTSE1d :: Int -> Job (V 1 (BigFloat 512))
testDynamicalSdpStressTSE1d numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing}
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax  = toV 1.060
      , boundingBoxMin  = toV 1.067
      , useExactHessian = True
      }
    externalStart = toV (1.0623203571743762)
    bound :: V 1 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V1 tttB) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024} -- SDPB.procsPerNode = 127}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 2
        paramNmax = 2
        dExtInit = toV (0.5182048561852542, 1.4131368185355069)
        lambdaInit = toV (realToFrac tttB, 0.015139242383650098, 1.1024431318572037, 1.6071717596745363, 1)


---------------
-- 6D search --
---------------

nmax6GFFNavigatorHessian :: Matrix 6 6 (BigFloat 512)
nmax6GFFNavigatorHessian = M.fromListsUnsafe
              [ [ 6822647.645851178
                , -1.3254828500835508e7
                , 734508.8262836185
                , 781084.3476611554
                , -1588125.4135115487
                , 88686.64427070717
                ]
              , [ -1.3254828500835508e7
                , 2275106.642415262
                , -386997.9809434247
                , -459525.4542366966
                , 130698.2244454183
                , -59241.82314284184
                ]
              , [ 734508.8262836185
                , -386997.9809434247
                , 2149482.5992055787
                , 2292163.2112799725
                , -1154106.0002865726
                , 1943.9022666330864
                ]
              , [ 781084.3476611554
                , -459525.4542366966
                , 2292163.2112799725
                , 2515341.080289049
                , -1274880.6566833104
                , 1552.272617490573
                ]
              , [ -1588125.4135115487
                , 130698.2244454183
                , -1154106.0002865726
                , -1274880.6566833104
                , 221161.69320246798
                , -18165.50411681199
                ]
              , [ 88686.64427070717
                , -59241.82314284184
                , 1943.9022666330864
                , 1552.272617490573
                , -18165.50411681199
                , 4051.592438080699
               ]
              ]


nmax6OptimumHessian :: Matrix 6 6 (BigFloat 512)
nmax6OptimumHessian = M.fromListsUnsafe
 [
  [2.3827738275344527589e6,-449253.3720810701411,257178.8433363715603,254239.99943068067011,-328069.14053524467049,59939.80126555849382],
  [-449253.3720810701411,92956.966493990440707,-44880.378095188893202,-45437.497657017457757,63127.325074462525011,-11090.216882788315127],
  [257178.8433363715603,-44880.378095188893202,45322.722371236798376,45413.922000491421649,-46852.251218004023342,6624.4615210227975627],
  [254239.99943068067011,-45437.497657017457757,45413.922000491421649,46488.622543246962047,-47248.347814210220447,6671.9965512229544797],
  [-328069.14053524467049,63127.325074462525011,-46852.251218004023342,-47248.347814210220447,54378.084070411564745,-8267.8897006176173767],
  [59939.80126555849382,-11090.216882788315127,6624.4615210227975627,6671.9965512229544797,-8267.8897006176173767,1599.2610550247752553]
 ]

nmax10OptimumHessian :: Matrix 6 6 (BigFloat 512)
nmax10OptimumHessian = M.fromListsUnsafe
                        [
                         [4.9884702670104361455258011695053274549274996976e8
                         , -4.152621250941081442003271486812763129127105461e7
                         , -6.85185939353608306139011974243297773201091966e6
                         , -995436.79865366185151128664360210946769454072
                         , 8.21394905631548286772256564817096561943965722e6
                         , 5.68159511353273009595829263420019839686149513e6]
                         ,
                         [-4.152621250941081442003271486812763129127105461e7
                         , 2.257587966736404751634003298227925718106986162e7
                         , -1.267060839847077502502901625380708149748238647e7
                         , -1.203867732473472907207660657981113509387848184e7
                         , 1.935563095459744910750892696717647814419660603e7
                         , 157421.26392908439659795303014652236199165783]
                         ,
                         [-6.85185939353608306139011974243297773201091966e6
                         , -1.267060839847077502502901625380708149748238647e7
                         , 3.923896513451817901648945778124552138420027418e7
                         , 3.778662518751734615412084904349313802741057706e7
                         , -3.685204438490650185548283718825517342222682954e7
                         , -959418.06377303098749706885158395575480714176]
                         ,
                         [-995436.79865366185151128664360210946769454072
                         , -1.203867732473472907207660657981113509387848184e7
                         , 3.778662518751734615412084904349313802741057706e7
                         , 3.853078547473150967979813929875935461185384301e7
                         , -3.500512361062182001456418759781964207158561049e7
                         , -742851.65734007267118124408317382768801826674]
                         ,
                         [8.21394905631548286772256564817096561943965722e6
                         , 1.935563095459744910750892696717647814419660603e7
                         , -3.685204438490650185548283718825517342222682954e7
                         , -3.500512361062182001456418759781964207158561049e7
                         , 3.860111824579459960479117921562418678808203503e7
                         , 1.05152905258408314794034110808835664956173612e6]
                         ,
                         [5.68159511353273009595829263420019839686149513e6
                         , 157421.26392908439659795303014652236199165783
                         , -959418.06377303098749706885158395575480714176
                         , -742851.65734007267118124408317382768801826674
                         , 1.05152905258408314794034110808835664956173612e6
                         , 259682.4673508485431721044586608518669733853]
                        ]


nmax10MaxSigHessian :: Matrix 6 6 (BigFloat 512)
nmax10MaxSigHessian = M.fromListsUnsafe
                     [
                       [2.236365604609754342637697349569210939545586438e9,
                       -2.242323701730556709887609153170161840543152775e8,
                       3.430842396529023115680915206477369788169549302e7,
                       4.228601927135601072907212703253109621510139508e7,
                       -4.243859072331985129834618717585788316299937674e7,
                       2.287984967919447798242026976791204521077323377e7],
                       [-2.242323701730556709887609153170161840543152775e8,
                       4.579617724500102490465659576500343872208236888e7,
                       -2.1572621032791681193487624864457744994313115e7,
                       -2.079737471541695900276440708072422930947133778e7,
                       3.01679580260970925788167337700232255833324206e7,
                       -1.69760043074837997222149572965601142006241126e6],
                       [3.430842396529023115680915206477369788169549302e7,
                       -2.1572621032791681193487624864457744994313115e7,
                       4.606787867370364466010055390659977360092955652e7,
                       4.428891767043339558191073238297447769327459634e7,
                       -4.469078498909792246930729083992863718602443338e7,
                       -603906.1022386256838912001698345288537467154],
                       [4.228601927135601072907212703253109621510139508e7,
                       -2.079737471541695900276440708072422930947133778e7,
                       4.428891767043339558191073238297447769327459634e7,
                       4.470580873258827891102978397205336763782961781e7,
                       -4.245748099310809820167075696223310648550769776e7,
                       -382476.54180651556919382062871575277685706776],
                       [-4.243859072331985129834618717585788316299937674e7,
                       3.01679580260970925788167337700232255833324206e7,
                       -4.469078498909792246930729083992863718602443338e7,
                       -4.245748099310809820167075696223310648550769776e7,
                       4.779080648000572417124652118827794211167063747e7,
                       543927.88617321450730303644735613695912899277],
                       [2.287984967919447798242026976791204521077323377e7,
                       -1.69760043074837997222149572965601142006241126e6,
                       -603906.1022386256838912001698345288537467154,
                       -382476.54180651556919382062871575277685706776,
                       543927.88617321450730303644735613695912899277,
                       460321.83532120642730587113889464583540477317]
                     ]



nmax14IntermHessian :: Matrix 6 6 (BigFloat 512)
nmax14IntermHessian = M.fromListsUnsafe
                      [
                       [2.33767876371520215041112328934685248259813820953e9
                      ,-4.6564950461774284140091101508922924307705063319e8
                      ,4.5752554204481958106171227531094664312236730127e8
                      ,4.4640944179384987075422953770895292182179248665e8
                      ,-4.8726902491724497533159509926771068485595614164e8
                      ,3.892541534974659207007980519115500739560806171e7]
                      ,
                       [-4.6564950461774284140091101508922924307705063319e8
                      ,1.382787957763620589732132795327338961826290577e8
                      ,-6.537101614708254332300980329039722245523632047e7
                      ,-7.403990142071733315867861808803933544838324597e7
                      ,9.989595053755767362624710930537296131160346327e7
                      ,-5.30957100359437541166317759435400746456241468e6]
                      ,
                       [4.5752554204481958106171227531094664312236730127e8
                      ,-6.537101614708254332300980329039722245523632047e7
                      ,1.7642324355848292405982639015645525998825970119e8
                      ,1.8370207588359360986221010297972025260803904247e8
                      ,-1.4788502148005250792154066033396629900495352435e8
                      ,9.61013385672418938349570830942028743164703443e6]
                      ,
                       [4.4640944179384987075422953770895292182179248665e8
                      ,-7.403990142071733315867861808803933544838324597e7
                      ,1.8370207588359360986221010297972025260803904247e8
                      ,2.017278116097610318639709929717663965108346307e8
                      ,-1.5767074267304521440050427094087478197205201128e8
                      ,9.49179908116876880082199797530807385532581559e6]
                      ,
                       [-4.8726902491724497533159509926771068485595614164e8
                      ,9.989595053755767362624710930537296131160346327e7
                      ,-1.4788502148005250792154066033396629900495352435e8
                      ,-1.5767074267304521440050427094087478197205201128e8
                      ,1.4231347581155936360952605499851317468201060617e8
                      ,-8.43397070118012463420449097058966736329912774e6]
                      ,
                       [3.892541534974659207007980519115500739560806171e7
                      ,-5.30957100359437541166317759435400746456241468e6
                      ,9.61013385672418938349570830942028743164703443e6
                      ,9.49179908116876880082199797530807385532581559e6
                      ,-8.43397070118012463420449097058966736329912774e6
                      ,1.10566845064268526124357113226185806789235932e6]
                      ]



nmax14OptimumHessian :: Matrix 6 6 (BigFloat 512)
nmax14OptimumHessian = M.fromListsUnsafe
                       [
                         [5.33822968493736308990535407923604269821127804135e9,
                         -1.0404259540918568825113033122196230061750275411e9,
                         7.9831085998744426834515049168190803575489626171e8,
                         7.8770757106691349195644534185969122722959601706e8,
                         -9.1994930612170426651525738786715368135601430809e8,
                         1.2811888707732681309157265633528673733407477185e8],
                         [-1.0404259540918568825113033122196230061750275411e9,
                         2.6202016043156010302516378536266797370457626606e8,
                         -1.5541154340506000488764000226727655669297022223e8,
                         -1.5464667248229555308362523479795223941215757449e8,
                         2.0788396176606756944459138745808283989679425663e8,
                         -2.396462329758113884980910442741013765852155026e7],
                         [7.9831085998744426834515049168190803575489626171e8,
                         -1.5541154340506000488764000226727655669297022223e8,
                         1.9311748858084350031679219405877474984844505432e8,
                         1.9922959193450810184650472022557377313261381092e8,
                         -1.9262116133645675059821670031444379648581086365e8,
                         2.249469858543559756233758675931507317902410302e7],
                         [7.8770757106691349195644534185969122722959601706e8,
                         -1.5464667248229555308362523479795223941215757449e8,
                         1.9922959193450810184650472022557377313261381092e8,
                         2.1108730404849529869728886451710522947929818579e8,
                         -1.9697752417115012615555523125787859835388812897e8,
                         2.291758795277365297687986308290006598527531464e7],
                         [-9.1994930612170426651525738786715368135601430809e8,
                         2.0788396176606756944459138745808283989679425663e8,
                         -1.9262116133645675059821670031444379648581086365e8,
                         -1.9697752417115012615555523125787859835388812897e8,
                         2.1357492598332719167626368096383031685948321729e8,
                         -2.409971201496544850287224071328503548386223157e7],
                         [1.2811888707732681309157265633528673733407477185e8,
                         -2.396462329758113884980910442741013765852155026e7,
                         2.249469858543559756233758675931507317902410302e7,
                         2.291758795277365297687986308290006598527531464e7,
                         -2.409971201496544850287224071328503548386223157e7,
                         3.41323385072327925588461188665863211354098652e6]
                       ]




nmax14ExtStep  :: V 6 (BigFloat 512)
nmax14ExtStep  = toV (
                 4.806946891589285913246912537199655599080752317495e-10,
                 6.1072031145696250349750744875141656024450512729558e-9,
                 -9.152188556081665418087210421047298714360941857591e-9,
                 5.531769526804348475115307498399051196577854907704e-10,
                 -1.20999541097128251142293784400951352177456838776615e-8,
                 -7.997906438203245226769219348331092706943382812359e-10
                 )

nmax14Gradient :: V 6 (BigFloat 512)
nmax14Gradient = toV (
                  -0.266375250483757449782182161272691769301564844223327793734,
                  0.0440198626715681489396329322427707060513821204750246457537,
                  -0.0703263521329339078074929708979132035987407246386543307785,
                  -0.0722935265823354550058228314850265958183851886627206607375,
                  0.0649505498910372702239552542208418021147019316653077797097,
                  -0.0081384057460342310376056040609791788053318859277098960733
                   )

nmax6OptimumPoint :: V 6 (BigFloat 512)
nmax6OptimumPoint = toV (
                     0.518148416224086050823154506002
                  ,  1.41252772053320783733490051994
                  ,  1.06339476297038503806991169
                  ,  0.0149471471140644953345953469014
                  ,  1.10357815962569275732993330163
                  ,  1.60730385393591205530918406731
                  )


nmax10AllowedPoint :: V 6 (BigFloat 512)
nmax10AllowedPoint = toV (
                     0.51815015876093750, 1.4126412025078125
                   , 1.06324642405525311372458050861, 0.0149168439386245464646461077470
                   , 1.10333180442851928750079312954, 1.60748124961753687856028120220
                  )

nmax10MaxSigPoint :: V 6 (BigFloat 512)
nmax10MaxSigPoint = toV (
                       0.5181559569940867,
                       1.4127070918040916, 1.0631351621413323, 0.014930356738142692,
                       1.1032012857870441, 1.6074484568519805
                       )

nmax10OptimumPoint :: V 6 (BigFloat 512)
nmax10OptimumPoint = toV (
                         0.518148729102607347438326707834744823218795109297704113929,
                         1.4126229628722212051889322692889024796179493612718591537745,
                         1.0632757484400718206399107037374357954472825112534820366244,
                         0.0149157619030183781529173289606098376161319797161613603232,
                         1.103369154577297013857951484010919091744929167982174825101,
                         1.6074847929315134155956780124934935980883591679855138925811
                     )



nmax14IntermPoint :: V 6 (BigFloat 512)
nmax14IntermPoint = toV (
                          0.5181488462468902308976029846163045792412317387536833746616
                        , 1.4126247003852506443373087781714593945561515382021610152754
                        , 1.0632731719315540591476182136303505924880173319216747551421
                        , 0.0149155485155413253095641998972947681178421995446819955043
                        , 1.1033657562307872160373892521096590070105090917419512238719
                        , 1.6074871886967038147286610956096447429719736481339031017377
           )



nmax14OptimumPoint :: V 6 (BigFloat 512)
nmax14OptimumPoint = toV (
                          0.51814879347865339714807653351643071584,
                          1.41262502927818752379949558861928837728,
                          1.06327271904675412467661510506851034663,
                          0.01491529037689909378019209799968076678,
                          1.10336466371243625693346602811497503143,
                          1.60748815952065469427002084091445382938
                     )



nmax14GuessPoint :: V 6 (BigFloat 512)
nmax14GuessPoint = toV (
--                           0.51814879840987, 1.4126250886433016,
                           0.5181488096969039, 1.4126252439334632,
                           1.06327262999286342585421597499217066008,
                           0.01491529615451545436871301312254282518,
                           1.10336455357163526624225048240984300463,
                           1.60748815019348936008802615466464898094
                  )


nmax14MinSigPoint :: V 6 (BigFloat 512)
nmax14MinSigPoint = --toV (  0.5181486382694964, 1.4126231191161676, 1.0632758960460311, 0.014914950358537737, 1.103368535323245, 1.6074889432418638)
                   toV (
                          0.51814835937969021444136623665660044751,
                          1.41261944018177956075629979913000000479,
                          1.06328170099882872209251445952571604186,
                          0.01491486948858967313829368253532445724,
                          1.10337614767584105575758796226319684237,
                          1.60749022396421367644988494167664441131
                     )

nmax14MaxSigPoint :: V 6 (BigFloat 512)
nmax14MaxSigPoint = toV ( 0.518149112950244, 1.4126285409928945, 1.063266092626666, 0.014915964682433946, 1.1033565716374856, 1.6074866569145123)


nmax18GuessPointDelta :: V 2 (BigFloat 512)
nmax18GuessPointDelta = toV (
                          0.5181487934828117585021129060445315744224944967925412000864,
                          1.412625029336926951754398315910984547002009644668004227764
                        )

nmax18GuessPointOPE :: V 4 (BigFloat 512)
nmax18GuessPointOPE = toV (
                       1.0632767158428064591906057004693878523342053470569097769953,
                       0.0149149196415908648552542961324822610074968909736691101731,
                       1.1033695749769327038374102603118944391989746322351478430112,
                       1.6074891682360134069140634254297844889077171079699876658017
                      )



---------------
-- 6D search --
---------------

-- Search for Minimum --

testDynamicalSdpStressTSE :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSE numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = initCkPoint}
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    initCkPoint = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/vxLoM/Object_e2D-38NSmd708-mFMROQoaXx10OR6LBHmS7wUp8PohY/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax       = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin       = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , useExactHessian      = False
      , prevHessian          = Just nmax6OptimumHessian
      , bfgsPartialUpdate    = 1
      , dualityGapUpperLimit = 0.000001
      }
    externalStart = nmax6OptimumPoint --toV (0.5182048561852542, 1.4131368185355069, 1.0623203571743762, 0.015139242383650098, 1.1024431318572037, 1.6071717596745363)
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
         , spins        = Prelude.take 26 $ Params.gnyspinsNmax paramNmax  Prelude.++ [49]
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024, SDPB.maxRuntime = SDPB.RunForDuration (1*3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)

testDynamicalSdpStressTSEnmax10 :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax10 numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = initCkPoint4Nodes}
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    --initCkPoint = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/LVmWJ/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
    initCkPoint4Nodes = -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/lyHUY/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/pvDSO/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/VPnRg/Object_aaEu9C6feppmT7CybWckncsNLGx7yvycErWY0v9EA7k/ck"
                        -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/CxhHW/Object_MZxMUFfWmM9yCyT-PXTldikiMe5yVCEtVN_aDt509-0/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax       = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin       = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , useExactHessian      = False
      , prevHessian          = Just nmax10OptimumHessian
      , bfgsPartialUpdate    = 1
      , dualityGapUpperLimit = 0.01
      , stepMaxThreshold     = 0.6
      }
    externalStart = nmax10OptimumPoint --nmax10AllowedPoint
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (36 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 10
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)



testDynamicalSdpStressTSEnmax14 :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax14 numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) { Bound.initialCheckpointDir = initCK
                                                               }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax       = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin       = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , useExactHessian      = True
      , prevHessian          = Just nmax14OptimumHessian
      --, prevExternalStep   = Just nmax14ExtStep
      --, prevGradient       = Just nmax14Gradient
      , dualityGapUpperLimit = 5e-7
      , bfgsPartialUpdate    = 1
      , stepMaxThreshold     = 0.5
      , betaScanMin          = 0.1
      --, betaScanMax        = 0.9
      }
    externalStart = nmax14OptimumPoint
    initCK = -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/SAIqR/Object_pVmi-5sBPrzhZN5Z2YSifVZ_CtcgxFM-C6g7PRY3xE8/ck"
             -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/racdo/Object_pVmi-5sBPrzhZN5Z2YSifVZ_CtcgxFM-C6g7PRY3xE8/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/BayZf/Object_pVmi-5sBPrzhZN5Z2YSifVZ_CtcgxFM-C6g7PRY3xE8/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/BayZf/Object_pVmi-5sBPrzhZN5Z2YSifVZ_CtcgxFM-C6g7PRY3xE8/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/aSAsT/Object_GUYkmUoOmVUDZNDjFTVEublP4_VWgWhrY2NiRQ_rfDU/ck"
             -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/aSAsT/Object_GUYkmUoOmVUDZNDjFTVEublP4_VWgWhrY2NiRQ_rfDU/ck-51"
             -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/Tapzj/Object_ooS9TmxRChxeSyoPrpe5YQQkwHzX5IC-VSVCmEK1aNg/ck"
             -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/Tapzj/Object_ooS9TmxRChxeSyoPrpe5YQQkwHzX5IC-VSVCmEK1aNg/ck-66"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/QmkPb/Object__RQ7GTedpznkESp1eGEnAu-rZ4XbaZ4xMgbmL1OinZ8/ck-84"
             -- Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/bWZXU/Object_EZGf60VP-EdcwHF7y2NlLCDKPk73ftjx0x2eARHirzQ/ck-114"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/ifXGF/Object_p7mwyAl5j-piYVbZsp1kg8LcAz-BkSGuvlpczCF4m20/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-03/PogOl/Object_4_JgJy6O74FLoPBqROAB8Moxo3_Z_ed3awcQq5a-XlY/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-03/krbmW/Object_N4I7iNv6fQi1wcFDKVAL-9HVd0QfXd3SJMJLGzoaIN8/ck"
             --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-03/QPlbM/Object_FAmeTzn6dOpBBMYSbOCGJurom6FShq3B2FcGjoPH0Yg/ck"
             Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-03/HOauh/Object_8hcWmBiLNxNcIRcdDipz6Q-HfveD0IVgCq8SJbWxix0/ck"
    bound :: V 6 (BigFloat 512) -> Bound (Precision 960) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (48 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 14
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)



testDynamicalSdpStressTSEnmax18OPE :: Int -> Job (V 4 (BigFloat 512))
testDynamicalSdpStressTSEnmax18OPE numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) { Bound.initialCheckpointDir = initCK
--                                                               , Bound.blockDir          = "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/cyOFq/Object_Z77Ih87vRAkTtBZ3PG0MFF0pumvZzLpPKPgPGEvq3AY/blocks"
--                                                               , Bound.sdpDir = "
                                                               }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax       = toV (1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin       = toV (1.067, 0.016, 1.11, 1.615)
      , useExactHessian      = True
      --, prevHessian        = Just nmax6OptimumHessian
      , dualityGapUpperLimit =  0.1
      , bfgsPartialUpdate    = 1
      }
    externalStart = nmax18GuessPointOPE
    initCK = Nothing --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/XXKYb/Object_nu84Yyi9bSbVwHp23wHrwJaonBy7dMsYRF-s6gP0hCM/ck"
    bound :: V 4 (BigFloat 512) -> Bound (Precision 1024) TSigEps3d
    bound (L.V4 tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExt (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams paramNmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (24 * 3600)
                                                  , SDPB.initialMatrixScalePrimal   = 1e70
                                                  , SDPB.initialMatrixScaleDual     = 1e70
                                                  }
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 18
        paramNmax = nmax+4
        dExt = fmap realToFrac nmax18GuessPointDelta
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)


testDynamicalSdpStressTSEnmax18 :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax18 numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir)
        { Bound.initialCheckpointDir = initCK
        --, Bound.blockDir = "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/fPyPs/Object_nu84Yyi9bSbVwHp23wHrwJaonBy7dMsYRF-s6gP0hCM/blocks"
        }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax       = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin       = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , useExactHessian      = False
      , prevHessian          = Just nmax6OptimumHessian
      , dualityGapUpperLimit = 0.1
      , bfgsPartialUpdate    = 1
      }
    externalStart = nmax18GuessPointDelta L.++ nmax18GuessPointOPE
    initCK = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/XXKYb/Object_nu84Yyi9bSbVwHp23wHrwJaonBy7dMsYRF-s6gP0hCM/ck4"
    bound :: V 6 (BigFloat 512) -> Bound (Precision 1024) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax)
         { SDPB.precision                  = 1024
         , SDPB.maxRuntime                 = SDPB.RunForDuration (24 * 3600)
         , SDPB.feasibleCenteringParameter = 0.3
         }
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 18
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)




---- Search for Boundary --
testDynamicalSdpStressTSEnmax6Boundary :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax6Boundary numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = initCkPoint
                                                              }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    initCkPoint = Nothing --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/vxLoM/Object_e2D-38NSmd708-mFMROQoaXx10OR6LBHmS7wUp8PohY/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax        = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin        = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , useExactHessian       = False
      , prevHessian           = Just nmax6OptimumHessian
      , bfgsPartialUpdate     = 1
      , findBoundaryDirection = Just (toV (1,0,0,0,0,0))
      , dualityGapUpperLimit  =  0.01
      }
    externalStart = nmax6OptimumPoint --toV (0.5182048561852542, 1.4131368185355069, 1.0623203571743762, 0.015139242383650098, 1.1024431318572037, 1.6071717596745363)
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax nmax) { B3d.keptPoleOrder = 14 }
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 6
        paramNmax = 8
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)


testDynamicalSdpStressTSEnmax10MinSig :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax10MinSig numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) { Bound.initialCheckpointDir = initCkPoint4Nodes
                                                               }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    --initCkPoint = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/LVmWJ/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
    initCkPoint4Nodes = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/lyHUY/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/pvDSO/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/CxhHW/Object_MZxMUFfWmM9yCyT-PXTldikiMe5yVCEtVN_aDt509-0/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/xAhUu/Object_CQAcl44u-LNiyK4by9thDBseKn8_UVO_nsWiDYHYshc/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax        = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin        = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , findBoundaryDirection = Just (toV (-1,0,0,0,0,0))
      , useExactHessian       = False
      , prevHessian           = Just nmax10OptimumHessian
      , bfgsPartialUpdate     = 1
      , dualityGapUpperLimit  =  0.01
      , stepMaxThreshold      = 0.6
      }
    externalStart = nmax10OptimumPoint --nmax10AllowedPoint
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (36 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 10
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)

testDynamicalSdpStressTSEnmax10MaxSig :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax10MaxSig numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = initCkPoint4Nodes
                                                                 }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    --initCkPoint = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/LVmWJ/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
    initCkPoint4Nodes = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/lyHUY/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/pvDSO/Object_gIvKpftOinkgPyLx-PlkiCPYImF42fY9klumc1fe7JM/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-01/CxhHW/Object_MZxMUFfWmM9yCyT-PXTldikiMe5yVCEtVN_aDt509-0/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/UeyvS/Object_dAg5WCn8J3OyLgur6_zih3-3WlWby65UwLHXjwKr0_E/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/jqwso/Object_CQAcl44u-LNiyK4by9thDBseKn8_UVO_nsWiDYHYshc/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/jlcWH/Object_CQAcl44u-LNiyK4by9thDBseKn8_UVO_nsWiDYHYshc/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/wWdvN/Object_CQAcl44u-LNiyK4by9thDBseKn8_UVO_nsWiDYHYshc/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax        = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin        = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , findBoundaryDirection = Just (toV (1,0,0,0,0,0))
      , useExactHessian       = True
      --, prevHessian         = Just nmax10OptimumHessian --nmax10MaxSigHessian
      , bfgsPartialUpdate     = 1
      , dualityGapUpperLimit  =  0.00008
      , stepMaxThreshold      = 0.5
      }
    externalStart = toV (0.5181563862683054401901717909102813227197584514679135014549,
                         1.412693661133009556280091561219061735123175148870641915206,
                         1.0631577563275527220167704217366013816329746481913488953789,
                         0.0149324174782946063872331152487823894621032786493457647672,
                         1.1032382599290569092194104720408040276470412105343132771202,
                         1.6074444433915017600825869338028023557464755126104762846145) --nmax10MaxSigPoint --nmax10AllowedPoint
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (36 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 10
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)

testDynamicalSdpStressTSEnmax14MinSig :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax14MinSig numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) { Bound.initialCheckpointDir = initCkPoint4Nodes
--    , Bound.blockDir          = "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/gLHjw/Object_u2uVgUleqeS8odQw7dWBh_1SWrTUNcVtmILRgJnaoTo/blocks"
                                                               }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    initCkPoint4Nodes = --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/JbbCQ/Object_Gt_LIE6t624xXsTHc2L3zHgl4MPuvDQ41DfUb6JzdO0/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/QZRRr/Object_Gt_LIE6t624xXsTHc2L3zHgl4MPuvDQ41DfUb6JzdO0/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/gLHjw/Object_u2uVgUleqeS8odQw7dWBh_1SWrTUNcVtmILRgJnaoTo/ck"
                        --Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/ZsDGL/Object_1fmEhHp0zmo4Bt7U0gjkaCc6rTYb7Rmyj0fCZpgDtTw/ck"
                        Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-03/TIaHQ/Object_9yoyZZjtHNOvh0HEl_KD-qvZvmQdhOZlZiz7n9X9rhE/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax        = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin        = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , findBoundaryDirection = Just (toV (-1,0,0,0,0,0))
      , useExactHessian       = True
      --, prevHessian         = Just nmax10OptimumHessian
      , bfgsPartialUpdate     = 1
      , dualityGapUpperLimit  =  0.000005
      , stepMaxThreshold      = 0.1
      , betaScanMin           = 0.3
      }
    externalStart = nmax14MinSigPoint
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (36 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 14
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)

testDynamicalSdpStressTSEnmax14MaxSig :: Int -> Job (V 6 (BigFloat 512))
testDynamicalSdpStressTSEnmax14MaxSig numIters = do
  workDir <- newWorkDir (bound externalStart)
  let stressNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = initCkPoint4Nodes
, Bound.blockDir          = "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/wFVUM/Object_c9b6Y1gOdtUDX2judkaoYZAp6jl_GgsiQKtaofYYksU/blocks"
                                                                 }
  skydiveBoundLoop (1*minute) (MkSkydiveState [] stressNavBoundFiles dynConfig externalStart defaultExactHessianClock) bound numIters
  where
    initCkPoint4Nodes = Just "/expanse/lustre/scratch/aikeliu/temp_project/data/2024-02/jjYLs/Object_c9b6Y1gOdtUDX2judkaoYZAp6jl_GgsiQKtaofYYksU/ck"
    dynConfig = defaultSkydiveConfig
      { boundingBoxMax        = toV (0.5180, 1.411, 1.060, 0.014, 1.05, 1.595)
      , boundingBoxMin        = toV (0.5183, 1.415, 1.067, 0.016, 1.11, 1.615)
      , findBoundaryDirection = Just (toV (1,0,0,0,0,0))
      , useExactHessian       = True
      --, prevHessian         = Just nmax10OptimumHessian --nmax10MaxSigHessian
      , bfgsPartialUpdate     = 1
      , dualityGapUpperLimit  =  0.00001
      , stepMaxThreshold      = 0.6
      , betaScanMin           = 0.5
      }
    externalStart = nmax14MaxSigPoint
    bound :: V 6 (BigFloat 512) -> Bound (Precision 768) TSigEps3d
    bound (L.V6 dSig dEps tttB tttF sse eee) = Bound
       { boundKey = (tSigEpsNavigatorDefaultGaps dExtInit (Just lambdaInit) paramNmax)
         { blockParams  = (Params.block3dParamsNmax paramNmax) {B3d.nmax=nmax}
         , spins        = Params.gnyspinsNmax paramNmax
         }
       , precision = MkPrecision
       , solverParams = (optimizationParams nmax) { SDPB.precision = 1024
                                                  , SDPB.maxRuntime = SDPB.RunForDuration (36 * 3600)}
       , boundConfig = defaultBoundConfig
       }
      where
        nmax = 14
        paramNmax = nmax+4
        dExtInit = toV (realToFrac dSig, realToFrac dEps)
        lambdaInit = toV (realToFrac tttB, realToFrac tttF, realToFrac sse, realToFrac eee, 1)

boundsProgram :: Text -> Cluster ()

boundsProgram "testDynamicalSdpStressTSE1d" =
--  local (setJobType (MPIJob 1 127) . setJobTime (168*hour) . setJobMemory "0G" . setSlurmQos "shared-oneweek" . setSlurmPartition "shared") $ do
  local (setJobType (MPIJob 1 128) . setJobTime (4*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSE1d 1)
  Log.info "This is the result" result


boundsProgram "testDynamicalSdpStressTSE" =
--  local (setJobType (MPIJob 1 127) . setJobTime (168*hour) . setJobMemory "0G" . setSlurmQos "shared-oneweek" . setSlurmPartition "shared") $ do
  local (setJobType (MPIJob 1 128) . setJobTime (12*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSE 1)
  Log.info "This is the result" result


boundsProgram "testDynamicalSdpStressTSEnmax6Boundary" =
--  local (setJobType (MPIJob 1 127) . setJobTime (168*hour) . setJobMemory "0G" . setSlurmQos "shared-oneweek" . setSlurmPartition "shared") $ do
  local (setJobType (MPIJob 1 128) . setJobTime (10*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax6Boundary 300)
  Log.info "This is the result" result


boundsProgram "testDynamicalSdpStressTSEnmax10" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
--  local (setJobType (MPIJob 1 127) . setJobTime (168*hour) . setJobMemory "0G" . setSlurmQos "shared-oneweek" . setSlurmPartition "shared") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax10 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax10MaxSig" =
  local (setJobType (MPIJob 4 128) . setJobTime (10*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax10MaxSig 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax10MinSig" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax10MinSig 300)
  Log.info "This is the result" result


boundsProgram "testDynamicalSdpStressTSEnmax14" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax14 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax14MaxSig" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax14MaxSig 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax14MinSig" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax14MinSig 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax18OPE" =
  local (setJobType (MPIJob 10 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax18OPE 50)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpStressTSEnmax18" =
  local (setJobType (MPIJob 4 128) . setJobTime (48*hour) . setJobMemory "0G" . setSlurmPartition "compute") $ do
  result <- remoteEvalJob $ static (testDynamicalSdpStressTSEnmax18 1)
  Log.info "This is the result" result


boundsProgram p = unknownProgram p

