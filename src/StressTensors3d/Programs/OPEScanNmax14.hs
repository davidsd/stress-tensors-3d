{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}

module StressTensors3d.Programs.OPEScanNmax14 where

import Bootstrap.Math.Linear                      (toV)
import Control.Monad.Reader                       (local)
import Data.Text                                  (Text)
import Hyperion                                   (Cluster, MPIJob (..),
                                                   setJobMemory, setJobTime,
                                                   setJobType)
import Hyperion.Bootstrap.Main                    (unknownProgram)
import Hyperion.Util                              (hour)
import Linear.V                                   (V)
import StressTensors3d.OPEScan                    qualified as OPEScan
import StressTensors3d.Programs.Defaults          (defaultDelaunayConfig)
import StressTensors3d.Programs.OPEScanNmax14Data (allowedPointsNmax14,
                                                   disallowedPointsNmax14,
                                                   isingSigEpsAffineNmax14)
import StressTensors3d.Programs.OPEScanNmax18Data (allowedPointsNmax18)
import StressTensors3d.Programs.TSigEps2023Data   (isingSigEpsAffineNmax22,
                                                   opeEllipseNmax10)
import StressTensors3d.Programs.TSigEpsOPEScan    (TSigEpsDelaunayConfig (..),
                                                   TSigEpsOPEScanConfig (..),
                                                   TSigEpsParams (..),
                                                   tSigEpsDelaunaySearch,
                                                   tSigEpsOPEScan)

paramsNmax14 :: TSigEpsParams
paramsNmax14 = MkTSigEpsParams
  { maxSpin   = 75
  , order     = 106
  , numPoles  = 35
  , precision = 832
  , nmax      = 14
  }

initialBilinearFormsNmax14 :: OPEScan.BilinearForms 5
initialBilinearFormsNmax14 = OPEScan.BilinearForms 1e-32 [(Nothing, opeEllipseNmax10)]

testOPEScanConfigNmax14 :: Maybe (V 5 Rational) -> Maybe FilePath -> [FilePath] -> TSigEpsOPEScanConfig
testOPEScanConfigNmax14 initialLambda initialCheckpoint statFiles = MkTSigEpsOPEScanConfig
  { initialBilinearForms = initialBilinearFormsNmax14
  , initialLambda        = initialLambda
  , affine               = isingSigEpsAffineNmax22
  , params               = paramsNmax14
  , initialCheckpoint    = initialCheckpoint
  , statFiles            = statFiles
  , maxSimultaneousJobs  = Nothing
  }

tSigEpsDelaunayConfigNmax14 :: Int -> Int -> [V 2 Rational] -> Maybe FilePath -> [FilePath] -> TSigEpsDelaunayConfig
tSigEpsDelaunayConfigNmax14 nThreads nSteps initialTestPoints initialCheckpoint statFiles = MkTSigEpsDelaunayConfig
  { opeScanConfig = MkTSigEpsOPEScanConfig
    { initialBilinearForms = initialBilinearFormsNmax14
    , initialLambda        = Just $ snd $ head allowedPointsNmax14
    , affine               = isingSigEpsAffineNmax14
    , params               = paramsNmax14
    , initialCheckpoint    = initialCheckpoint
    , statFiles            = statFiles
    , maxSimultaneousJobs  = Nothing
    }
  , initialAllowedPoints    = fmap fst allowedPointsNmax14
  , initialDisallowedPoints = disallowedPointsNmax14
  , initialTestPoints       = initialTestPoints
  , delaunayConfig          = defaultDelaunayConfig nThreads nSteps
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "TSigEps_nmax_14_feasible_point_test" =
  local (setJobType (MPIJob 5 124) . setJobTime (48*hour) . setJobMemory "0G") $ do
  tSigEpsOPEScan (testOPEScanConfigNmax14 initialLambda initialCheckpoint statFiles) deltaExts
  where
    deltaExts = map toV
      [ (0.518148538017, 1.41262201095)
      , (0.518148420033, 1.41262043785)
      , (0.518148410125, 1.41262005039)
      , (0.518148525282, 1.41262132506)
      , (0.518148684414, 1.41262319339)
      , (0.51814912490844242907, 1.4126286828760428663) -- skydiving
    --, (0.518149110600, 1.41262872710) -- disallowed
    --, (0.518149043100, 1.41262818540) -- disallowed
    --, (0.518148910200, 1.41262676380) -- disallowed
    --, (0.518148762700, 1.41262500510) -- disallowed
    --, (0.518148656700, 1.41262358120) -- disallowed
    --, (0.518148633000, 1.41262303590) -- allowed
    --, (0.518148700400, 1.41262357750) -- allowed
    --, (0.518148833300, 1.41262499920) -- disallowed
    --, (0.518148980900, 1.41262675780) -- disallowed
    --, (0.518149086800, 1.41262818180) -- disallowed
    --, (0.518149110600, 1.41262872710) -- disallowed
    --, (0.51814882338193927145874808, 1.4126254772155267750296378) -- allowed
      ]
    initialLambda = Just $ snd $ head allowedPointsNmax18
      -- This is lambda at the the nmax=14 optimum
      -- Just $ toV
      -- ( 1.06327271904675412467661510507
      -- , 0.0149152903768990937801920979997
      -- , 1.10336466371243625693346602811
      -- , 1.60748815952065469427002084091
      -- , 1
      -- )
    initialCheckpoint = Just "/cosma8/data/dp328/dc-krav1/hyperion_stress_tensors_3d/data/2024-08/HvyDs/Object__U_eHuSaEhOga1luX0Ltbqhj8v97u6G9YBjzYOLwwAk/ck"
    statFiles = ["/cosma/apps/dp284/dc-krav1/repos/stress-tensors-3d/task_stats/TSigEps_test_nmax14_cosma8_5_128_collected_stats_order_106.json"]


boundsProgram "TSigEps_nmax_14_delaunay_search" =
  local (setJobType (MPIJob 5 128) . setJobTime (12*hour) . setJobMemory "0G") $
  tSigEpsDelaunaySearch $
  tSigEpsDelaunayConfigNmax14 3 30 initialTestPoints initialCheckpoint statFiles
  where
    initialCheckpoint = Just "/cosma8/data/dp328/dc-krav1/hyperion_stress_tensors_3d/data/2024-08/HvyDs/Object__U_eHuSaEhOga1luX0Ltbqhj8v97u6G9YBjzYOLwwAk/ck"
    statFiles = ["/cosma/apps/dp284/dc-krav1/repos/stress-tensors-3d/task_stats/TSigEps_test_nmax14_cosma8_5_128_collected_stats_order_106.json"]
    initialTestPoints = map toV
      [ -- (0.51814862049422056937547850,1.4126227533608918239171971)
      ]

boundsProgram p = unknownProgram p
