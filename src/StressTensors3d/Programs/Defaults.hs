{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE TemplateHaskell     #-}

module StressTensors3d.Programs.Defaults where

import Config qualified
import Data.FileEmbed                    (makeRelativeToProject, strToExp)
import Hyperion                          (HyperionConfig (..))
import Hyperion.Bootstrap.Bound          (BoundConfig (..))
import Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..),
                                          ChoiceMethod (..))
import QuadraticNet                      qualified as QN
import System.FilePath.Posix             ((</>))

defaultBoundConfig :: BoundConfig
defaultBoundConfig = BoundConfig Config.scriptsDir

defaultDelaunayConfig :: Int -> Int -> DelaunayConfig
defaultDelaunayConfig nThreads nSteps = DelaunayConfig
  { choiceMethod        = LongestMixedEdgeMidpoint
  , terminateTime       = Nothing
  , nThreads            = nThreads
  , nSteps              = nSteps
  , qdelaunayExecutable = Config.scriptsDir </> "qdelaunay.sh"
  }

defaultQuadraticNetConfig :: QN.QuadraticNetConfig
defaultQuadraticNetConfig = (QN.defaultQuadraticNetConfig
  (Config.scriptsDir </> "pmp2sdp.sh")
  (Config.scriptsDir </> "sdpb.sh")
  (Config.config.dataDir </> "quadratic-net"))
  { QN.precision       = 384 
  , QN.sdpBarrierShift = 1e-32
  }

taskStatsDir :: FilePath
taskStatsDir = $(makeRelativeToProject "task_stats" >>= strToExp)
