{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module StressTensors3d.Programs.SingleNodeScan where

import Control.Monad.IO.Class           (liftIO)
import Control.Monad.Reader             (asks)
import Data.Aeson                       (ToJSON)
import Data.Binary                      (Binary)
import Data.Time                        (NominalDiffTime)
import Hyperion                         (Cluster, Dict (..), Job, Static (..),
                                         cAp, cPure, clusterJobOptions,
                                         newWorkDir, remoteEvalJob)
import Hyperion.Bootstrap.Bound.Compute (cleanFilesWithTreatment,
                                         computeWithFileTreatment,
                                         setFixedTimeLimit)
import Hyperion.Bootstrap.Bound.Types   (Bound (..), BoundFileTreatment,
                                         BoundFiles, FileTreatment (..),
                                         SDPFetchBuildConfig, ToSDP,
                                         blockDirTreatment,
                                         initialCheckpointDir, jsonDirTreatment)
import Hyperion.Database                qualified as DB
import Hyperion.Log                     qualified as Log
import Hyperion.Slurm                   qualified as Slurm
import SDPB qualified
import Type.Reflection                  (Typeable)

-- | Compute a bound with a custom 'BoundFileTreatment', returning the
-- result and the 'BoundFiles'. This is for computing a bunch of
-- bounds with the same external dimensions in a way that reuses the
-- files.
remoteComputeAllWithFileTreatment
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => BoundFileTreatment
  -> (FilePath -> BoundFiles)
  -> Maybe FilePath
  -> [Bound Int b]
  -> Cluster ([SDPB.Output], BoundFiles)
remoteComputeAllWithFileTreatment treatment mkFiles initCk bounds = do
  let bound0 = head bounds
  workDir <- newWorkDir bound0
  jobTime <- asks (Slurm.time . clusterJobOptions)
  let boundFiles = mkFiles workDir
  results <-
    remoteEvalJob $
    static computeAll `cAp`
    closureDict `cAp`
    cPure treatment `cAp`
    cPure jobTime `cAp`
    cPure bounds `cAp`
    cPure boundFiles `cAp`
    cPure initCk
  Log.info "Computed" (zip bounds results)
  --need to clean since we're modifying the treatment in job
  Log.info "Cleaning files" boundFiles
  cleanFilesWithTreatment treatment boundFiles
  return (results, boundFiles)

computeAll
  :: Dict
     ( Show b
     , Typeable b
     , ToJSON b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  -> BoundFileTreatment
  -> NominalDiffTime
  -> [Bound Int b]
  -> BoundFiles
  -> Maybe FilePath
  -> Job [SDPB.Output]
computeAll Dict treatment jobTime bounds files initCk =
  mapM compSingle (zip (True : repeat False) bounds)
  where
    treatment' = treatment { blockDirTreatment = KeepFile , jsonDirTreatment  = KeepFile }
    files' = files { initialCheckpointDir = initCk }
    compSingle (changeCk, bound) = do
      solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams bound)
      let bound' = bound { solverParams = solverParams' }
      result <- computeWithFileTreatment treatment' bound' (if changeCk then files' else files)
      DB.insert (DB.KeyValMap "computations") bound result
      return result
