{-# LANGUAGE DataKinds #-}

-- | This module defines routines for automatically constructing
-- crossing equations involving stress tensors and scalars.

module StressTensors3d.CrossingEqs
  ( HasT (..)
  , crossingEqsStructSet
  , crossingEqsStructSetList
  , structsTTTT
  , structsTTTS
  , structsTTSS
  , structsTSSS
  , structsSSSS
  ) where

import Blocks                                    (Coordinate (XT), Taylors,
                                                  xEven, xOdd, xtTaylors,
                                                  xtTaylorsConstant,
                                                  xtTaylorsRadial, yEven)
import Blocks.Blocks3d                           qualified as B3d
import Bootstrap.Bounds                          (FourPointFunctionTerm, Tuple4)
import Bootstrap.Bounds.Crossing.UnorderedTuples (toTuple2, uPair)
import Bootstrap.Math.FreeVect                   (FreeVect)
import Bootstrap.Math.HalfInteger                (HalfInteger)
import Bootstrap.Math.Linear                     (fromRawVector)
import Data.List.Extra                           (nubOrd)
import Data.Vector                               qualified as Vector
import GHC.TypeNats                              (KnownNat)
import Linear.V                                  (V)

-- | A class for an operator type 'o' that includes a stress tensor
class HasT o where
  stressTensor :: o

data DerivType
  = BulkDerivs
  | RadialDerivs
  | ConstantTerm

-- | Given a 'DerivType' and a list of correlators (with operators
-- decorated by their q-values), build the corresponding crossing
-- equations by taking even/odd linear combinations of swapping 1<->3,
-- with appropriate even/odd x-derivatives.
--
-- TODO: Don't specialize to yEven?
crossingEqs
  :: (Num a, Eq a, Ord b, Ord o)
  => DerivType
  -> [Tuple4 (o, HalfInteger)]
  -> FourPointFunctionTerm o B3d.Q4Struct b a
  -> [(Taylors 'XT, FreeVect b a)]
crossingEqs derivType correlators g0 =
  concatMap mkCrossing $ nubOrd $ map symmetrize13 correlators
  where
    taylors = case derivType of
      BulkDerivs   -> flip xtTaylors yEven
      RadialDerivs -> xtTaylorsRadial
      ConstantTerm -> const xtTaylorsConstant -- not actually used
    symmetrize13 (o1,o2,o3,o4) = (uPair o1 o3, o2, o4)
    mkCrossing (o13, o2, o4) = case toTuple2 o13 of
      (o1,o3) -> case derivType of
        ConstantTerm | o1 == o3 -> []
        ConstantTerm -> [ (xtTaylorsConstant, g o1 o2 o3 o4 - g o3 o2 o1 o4) ]
        _ | o1 == o3 -> [ (taylors xOdd,      g o1 o2 o3 o4) ]
        _            -> [ (taylors xOdd,      g o1 o2 o3 o4 + g o3 o2 o1 o4)
                        , (taylors xEven,     g o1 o2 o3 o4 - g o3 o2 o1 o4)
                        ]
    g (o1,q1) (o2,q2) (o3,q3) (o4,q4) =
      g0 o1 o2 o3 o4 (B3d.Q4Struct (q1,q2,q3,q4) yEven)

-- | Given a Tuple4 of operators, assign the given list of q's to the
-- T's in the tuple in order. Other operators are assigned q=0.
applyTStructs
  :: (Eq o, HasT o)
  => Tuple4 o
  -> [HalfInteger] -- ^ Must have length >= the number of T's in the tuple
  -> Tuple4 (o, HalfInteger)
applyTStructs (o1,o2,o3,o4) = toTuple4 . go [o1,o2,o3,o4]
  where
    go [] _                                  = []
    go (t : os) (q : qs) | t == stressTensor = (t,q) : go os qs
    go (t : _) []        | t == stressTensor = error "applyTStructs: Not enough qs in structure"
    go (o : os) qs                           = (o,0) : go os qs
    toTuple4 [a,b,c,d] = (a,b,c,d)
    toTuple4 _         = error "toTuple4: Wrong number of elements"

type StructSet = [(DerivType, [[HalfInteger]])]

-- | Independent derivatives for correlators with stress tensors and
-- scalars. Taken from Alexandre's notebook sTe_structures.nb
structsTTTT, structsTTTS, structsTTSS, structsTSSS, structsSSSS :: StructSet
structsTTTT =
  [ ( BulkDerivs
    , [[2,2,2,2],[1,1,1,1],[2,1,1,2],[1,2,1,2],[1,1,2,2]]
    )
  , ( RadialDerivs
    , [[0,0,0,0],[0,1,0,1],[0,2,0,2],[0,1,1,2]
      ,[1,1,0,2],[0,0,1,1],[0,0,-1,1],[1,0,0,1],[-1,0,0,1]]
    )
  , ( ConstantTerm
    , [[2,0,0,2],[1,-1,0,2],[1,-1,-1,1],[0,1,-1,2]
      ,[0,0,2,2],[0,-1,1,2],[-1,1,0,2],[-1,-1,1,1]]
    )
  ]
structsTTTS =
  [ (BulkDerivs, [[2,2,2],[1,2,1],[1,1,2],[2,1,1]])
  , (RadialDerivs, [[2,0,0],[0,0,2],[0,2,0]])
  , (ConstantTerm, [[2,2,-2],[-2,2,2]])
  ]
structsTTSS = [(BulkDerivs, [[2,2], [1,1]]), (RadialDerivs, [[0,0]])]
structsTSSS = [(BulkDerivs, [[2]])]
structsSSSS = [(BulkDerivs, [[]])]

-- | Construct a list of crossing equations from a list of correlators
-- and their corresponding 'StructSet's
crossingEqsStructSetList
  :: (Ord b, Num a, Eq a, HasT o, Ord o)
  => [(Tuple4 o, StructSet)]
  -> FourPointFunctionTerm o B3d.Q4Struct b a
  -> [(Taylors 'XT, FreeVect b a)]
crossingEqsStructSetList correlators g = do
  (c, structs) <- correlators
  (derivType, qs) <- structs
  crossingEqs derivType (map (applyTStructs c) qs) g

-- | Convert a List to a Vector of the given length. Throws an
-- exception if the List has the wrong length.
coerceV :: KnownNat n => [a] -> V n a
coerceV = fromRawVector . Vector.fromList

-- | This function coerces the list of crossing equations to a Vector
-- with a statically-known length. If the length is incorrect, an
-- exception will be thrown at runtime. The intended use-case is that
-- one computes the crossing equations using
-- 'crossingEqsStructSetList', computes its length, and then
-- hard-codes in the length by switching 'crossingEqsStructSetList' ->
-- 'crossingEqsStructSet'.
crossingEqsStructSet
  :: (Ord b, Num a, Eq a, HasT o, Ord o, KnownNat n)
  => [(Tuple4 o, StructSet)]
  -> FourPointFunctionTerm o B3d.Q4Struct b a
  -> V n (Taylors 'XT, FreeVect b a)
crossingEqsStructSet correlators = coerceV . crossingEqsStructSetList correlators
