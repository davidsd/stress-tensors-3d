#!/bin/bash

# # Default value
# ntasks=64

# # Set ntasks from the argument list and remove it from args if it exists
# args=("$@")
# for ((i=0; i<"${#args[@]}"; ++i)); do
#     case ${args[i]} in
#         --ntasks)
#             ntasks="${args[i+1]}";
#             unset args[i];
#             unset args[i+1];
#             break
#             ;;
#     esac
# done

source ~/.zprofile
echo "hee hee I am in rajeev's config. here's some infiniband facts™"
ibv_devinfo

# module load cpu gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7
module list
ompi_info
ulimit -c unlimited
# echo srun -v /home/vdommes/install/sdpb-master/bin/sdp2input $@
# srun -v /home/vdommes/install/sdpb-master/bin/sdp2input  $@

echo srun -v /home/vdommes/install/sdpb-new-pmp-sampling/bin/pmp2sdp $@
srun -v /home/vdommes/install/sdpb-new-pmp-sampling/bin/pmp2sdp $@
# echo mpirun --mca btl_base_verbose 100 -n "$ntasks" /home/wlandry/gnu_openmpi/install/bin/sdp2input "${args[@]}"
# mpirun --mca btl_base_verbose 100 -n "$ntasks" /home/wlandry/gnu_openmpi/install/bin/sdp2input "${args[@]}"

