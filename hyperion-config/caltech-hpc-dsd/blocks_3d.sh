#!/bin/bash

module purge
module load cmake/3.25.1 gcc/9.2.0 openmpi/4.1.5 boost/1_81_0_openmpi-4.1.1_gcc-9.2.0 eigen/eigen mpfr/4.0.2

# Chat GPT extracts the param_file argument:

command_args=("$@")

# Initialize an empty variable for the parameter file
param_file=""

# Loop through all arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --param-file) # Check for the --param-file argument
            param_file="$2" # Assign the next argument as the value of param_file
            shift # Move past the argument value
            ;;
        *) # Ignore any other arguments
           # Optionally, you can add code here to handle other arguments
            ;;
    esac
    shift # Move to the next argument
done


ulimit -c unlimited

echo cat $param_file
cat $param_file
echo /home/dssimmon/install/bin/blocks_3d "${command_args[@]}"
/home/dssimmon/install/bin/blocks_3d "${command_args[@]}"
