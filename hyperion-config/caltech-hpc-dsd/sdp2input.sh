#!/bin/bash

module purge
module load cmake/3.25.1 gcc/9.2.0 openmpi/4.1.5 boost/1_81_0_openmpi-4.1.1_gcc-9.2.0 eigen/eigen mpfr/4.0.2

echo /home/vdommes/install/sdpb-master/bin/sdp2input $@
/home/vdommes/install/sdpb-master/bin/sdp2input $@
