#!/bin/bash

source /etc/profile.d/01-lmod.sh

module purge
module restore bootstrap_modules
ulimit -c unlimited
echo mpirun pmp2sdp $@
mpirun pmp2sdp $@
