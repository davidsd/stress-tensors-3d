#!/bin/bash

echo $SHELL

source /etc/profile.d/01-lmod.sh

module purge
module restore bootstrap_modules

echo mpirun sdpb $@
mpirun sdpb $@
