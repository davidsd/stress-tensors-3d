#!/bin/bash

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/0.3.10-openmp ncurses slurm
ulimit -c unlimited
echo /home/vdommes/install/bin/scalar_blocks $@
/home/vdommes/install/bin/scalar_blocks $@
