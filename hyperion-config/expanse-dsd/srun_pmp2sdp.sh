#!/bin/bash

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm
ulimit -c unlimited
echo srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@
# Try up to 3 times because sometimes srun fails for no good reason
srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@ || \
    srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@ || \
    srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/pmp2sdp $@

