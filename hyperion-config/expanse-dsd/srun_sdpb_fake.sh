#!/bin/bash

# This is a fake version of srun_sdpb.sh that sets the
# dualityGapThreshold to 1e-45. This was needed for a job that crashed
# with not HPD before its goal of 1e-50, while 1e-45 would have been
# fine. It also removes --initialCheckpointDir if --checkpointDir
# already exists.

module purge
module load cpu/0.15.4 gcc/10.2.0 openmpi/4.0.4 gmp/6.1.2 mpfr/4.0.2 cmake/3.18.2 openblas/dynamic/0.3.7 boost/1.74.0 slurm

# Initialize an array to hold the new arguments
new_args=()
checkpoint_dir=""
remove_initial_checkpoint_dir=false

# Loop over all arguments
while [[ "$#" -gt 0 ]]; do
  if [[ $1 == "--dualityGapThreshold" && $2 == "1.0e-50" ]]; then
    # Replace --dualityGapThreshold with the new value if it is 1.0e-50
    new_args+=("--dualityGapThreshold")
    new_args+=("1.0e-45")
    shift 2  # Skip the original --dualityGapThreshold and its value
  elif [[ $1 == "--checkpointDir" ]]; then
    # Get the checkpoint directory value
    checkpoint_dir=$2
    new_args+=("--checkpointDir")
    new_args+=("$checkpoint_dir")
    shift 2  # Skip the --checkpointDir and its value
  elif [[ $1 == "--initialCheckpointDir" ]]; then
    # Flag to remove the --initialCheckpointDir if the checkpoint directory exists
    if [[ -d "$checkpoint_dir" ]]; then
      remove_initial_checkpoint_dir=true
    else
      new_args+=("--initialCheckpointDir")
      new_args+=("$2")
    fi
    shift 2  # Skip the --initialCheckpointDir and its value
  else
    # Add all other arguments to the new array
    new_args+=("$1")
    shift  # Move to the next argument
  fi
done

# If checkpointDir exists, remove --initialCheckpointDir from new_args
if [[ $remove_initial_checkpoint_dir == true ]]; then
  echo "The checkpoint directory exists. --initialCheckpointDir argument has been removed."
fi

echo srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/sdpb "${new_args[@]}"
srun -v /home/vdommes/install/sdpb-spectrum-mpsolve/bin/sdpb "${new_args[@]}"
